import numpy as np
from Mage.Files.file_RUG import file_RUG
from Mage.Files.file_PSI import file_PSI
from Mage.Files.file_LIM import file_LIM
from Mage.Files.file_HYD import file_HYD
# from Mage.Files.file_INI import file_INI

# To add:
# > CLM par bief
# RQS:
#     dans compute_section: calcul fait en se basant sur X (plutôt que pk): X0+slope*Z
#  fix to have unique id for sections
# id in ST files are not ranked

class Model:
    def __init__(self):
        self.name = ''
        self.bief = []
        self.path = ''

    def export_RUG(self):
        if len(self.bief) == 0:
            print('No bief')
            return

        for bf in self.bief:
            if len(bf.rug) == 0:
                print('No Stricklers. Define Strickler as [[(xmin,xmax),(Kmin,Kmoy)],]')
                return

        RUG_file = file_RUG()
        RUG_file.name = self.name + '.RUG'

        path_RUG = self.path + '/' + RUG_file.name
        with open(path_RUG, 'w') as file:
            file.write(RUG_file.write_file(self.bief))

    def export_PSI(self):
        if len(self.bief) == 0:
            print('No bief')
            return

        for bf in self.bief:
            if len(bf.psi) == 0:
                print('No Psi values. Define Psi as [[(xmin,xmax),Psi],]')
                return

        PSI_file = file_PSI()
        PSI_file.name = self.name + '.PSI'

        path_PSI = self.path + '/' + PSI_file.name
        with open(path_PSI, 'w') as file:
            file.write(PSI_file.write_file(self.bief))

    def export_HYD(self):

        if len(self.bief) == 0:
            print('No bief')
            return

        CLM = [(bf.hyd,bf.node_upstream,bf.hyd_time) for bf in self.bief if bf.hyd !=0]

        HYD_file = file_HYD()
        HYD_file.name = self.name + '.HYD'

        path_HYD = self.path + '/' + HYD_file.name

        with open(path_HYD, 'w') as file:
            [file.write(HYD_file.write_file(inlet[0],inlet[1],inlet[2])) for inlet in CLM]


    def export_LIM(self):

        if len(self.bief) == 0:
            print('No bief')
            return

        LIM = [(bf.lim,bf.node_downstream) for bf in self.bief if bf.lim !=0]

        LIM_file = file_LIM()
        LIM_file.name = self.name + '.LIM'

        path_LIM = self.path + '/' + LIM_file.name

        with open(path_LIM, 'w') as file:
            [file.write(LIM_file.write_file(inlet[0],inlet[1])) for inlet in LIM]

    # def export_INI(self):
    #     if len(self.bief) == 0:
    #         print('No bief')
    #         return
    #

