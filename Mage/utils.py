from Mage.Geometry.bief import bief
from Mage.Etude import Model


def to(pd):
    """
    To remove the initial delay
    :param pd:
    :return:
    """
    return pd.loc[pd.iloc[:,0]>=0].reset_index(drop=True)


def check_object(obj,type):
    if not isinstance(obj,type):
        print('{} is supposed to be type {}'.format(obj,type))
        return False
    return True


def get_Sfi(args):
    Bm, Bf, nm, nf, hm, hbk, Qf, Qm = args
    hf = hm - hbk
    Af = Bf * hf
    Am = Bm * hm
    Pf = Bf + hf
    Pm = Bm + 2 * hbk
    Rhf = Af / Pf
    Rhm = Am / Pm
    Sff = nf ** 2 * Qf ** 2 / Rhf ** (4 / 3)
    Sfm = nm ** 2 * Qm ** 2 / Rhm ** (4 / 3)
    return Sff, Sfm


def psi_t_pst(args, opt=1):
    Bm, Bf, nm, nf, hm, hbk, Qf, Qm = args
    hf = hm - hbk
    if opt == 1:
        Sff, Sfm = get_Sfi(args)
    else:
        Sff, Sfm = get_Sfi_Q(args)
    Am, Af = Bm * hm, Bf * hf
    Um, Uf = Qm / Am, Qf / Af
    g = 9.81

    denom = 2 / Am + 1 / Af
    psi = g * (Sff - Sfm) / (hf * (Um - Uf) ** 2) / denom
    return psi


def get_Sfi_Q(args):
    Bm, Bf, nm, nf, hm, hbk, Qf, Qm = args
    hf = hm - hbk
    Af = Bf * hf
    Am = Bm * hm
    Pf = Bf + hf
    Pm = Bm + 2 * hbk
    Rhf = Af / Pf
    Rhm = Am / Pm
    Sff = nf ** 2 * Qf ** 2 / Af ** 2 / Rhf ** (4 / 3)
    Sfm = nm ** 2 * Qm ** 2 / Am ** 2 / Rhm ** (4 / 3)
    return Sff, Sfm


def umbalance_V2(total,qfu,alpha):
    Qf=(1+alpha/100)*qfu
    Qm = total-2*Qf
    return (Qm,Qf)



# def umbalance_V3(total,qfu,alpha,time):
#     # To generalize from my experiment to general channel
#     Qf=(1+alpha/100)*qfu
#     Qm = total-Qf
#     return (Qf,Qm,0,time)


def Bief_CLM(path, Qt, Qfu, alpha,time=None):
    # on construit le bief
    Bief1 = bief()
    Bief1.node_upstream = 'aaa'
    Bief1.id = 1
    Bief1.hyd = umbalance_V2(Qt, Qfu, alpha)
    # Bief1.hyd = umbalance_V3(Qt, Qfu, alpha,time=None)
    Bief1.hyd_time=time
    PN = Model()
    PN.name = 'CM'
    PN.bief = [Bief1]
    PN.export_HYD(path)


def umbalance_unsteady(data, alpha):
    (Qf1, Qm, Qf2) = data
    Qfi1 = [(1 + alpha / 100) * qf for qf in Qf1]
    Qfi2 = [(1 + alpha / 100) * qf for qf in Qf2]
    Qt = [qm + qf1 + qf2 for qm, qf1, qf2 in zip(Qm, Qf1, Qf2)]
    Qmi = [qt-qf1-qf2 for qt,qf1,qf2 in zip(Qt,Qfi1,Qfi2) ]

    result = [(qf1, qm, qf2) for qf1, qm, qf2 in zip(Qfi1, Qmi, Qfi2)]
    return result


def Bief_CLM_unsteady(path, data, alpha, time,name):
    (Qf1, Qm, Qf2) = data
    Bief1 = bief()
    Bief1.node_upstream = 'aaa'
    Bief1.id = 1
    Bief1.hyd = umbalance_unsteady((Qf1, Qm, Qf2), alpha)
    Bief1.hyd_time = time

    PN = Model()
    PN.name = name
    PN.bief = [Bief1]
    PN.export_HYD(path)

