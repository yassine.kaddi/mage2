import pandas as pd
import numpy as np
from datetime import datetime
import glob
import os

g = 9.81
class Magefin:
    '''QQ infos à changer en cas de généralisation:
        >les *1000 pr laisser débit en m3/s et hauteurs en mm
        >le .117 différence entre mc et fp
        > Rayon hydraulique calculé pr mon cas
        > fichiers .csv (variation en fct du temps) calculées pr mon cas
        > je prends que Qf gauche et Qmineur'''

    def __init__(self,name):
        self.Steady = pd.DataFrame([])
        self.folders = {}
        self.option=''
        self.q_values = pd.DataFrame()
        self.last_time_step = 0
        self.model_name = name
        self.path = ''
        self.weight=0
        self.Z_DT = {}
        self.Q_DT = ()
        # self.lateral = pd.DataFrame()
        self.Rh_DT = {}

    def Read_Magefin(self, path, option='I'):
        """
        To read the mage_fin.fin file
        :param path: of the magefin file
        :param option: Debord or ISM. BY default it takes Debord
        :return: pandas of magefin file
        """
        # Debord
        L = np.concatenate((np.arange(2, 7), [18,19,20,21,16,14,15]))
        cols = ['Q(L/s)', 'Cote(m)', 'Pm(m)', 'Z_fond', 'Z_moy','Qm', 'Qf', 'Vm', 'Vf','Surface','Km','Kf']

        # ISM
        if option == 'I':
            L = np.concatenate(([0],np.arange(2, 7), [12, 13, 14, 15, 17, 18, 20, 21, 26,28]))
            cols = ['IB','Q(L/s)', 'Cote(m)', 'Pm(m)', 'Z_fond', 'Z_moy', 'Largeur', 'Fr', 'Kf', 'Km', 'Qf', 'Qm', 'Vf',
                    'Vm', 'tau','Uint']

        data = pd.read_csv(path + '/Mage_fin.ini', encoding="ISO-8859-1", sep="\s+", skiprows=9, usecols=L, names=cols)

        bankful = data['Z_moy'].iloc[-1]
        data['Hm'] = (data['Cote(m)'] - data['Z_fond']) * 1000
        Z_f = data['Z_fond'] + bankful
        data['Qr'] = data['Qf'] / data['Q(L/s)']
        data['Hf'] = (data['Cote(m)'] - Z_f) * 1000
        data['hr'] = data['Hf'] / data['Hm']
        data['Am'] = data['Qm']/data['Vm']
        data['Af'] = data['Qf']/data['Vf']
        data['Rh_f'] = data['Am']/ (data['Hf'] / 1000 + 1) / 1000
        data['Rh_m'] = data['Af'] / (data['Hm'] / 1000 + 1 + bankful) / 1000
        self.option=option
        return data

    def Read_one(self,path='',option='I'):
        # Read one simulation in path
        if len(path)==0:
            path = self.path


        self.Steady = self.Read_Magefin(path,option)

    def Read_many(self,path,option='I'):
        # Read many simulation folders except those with "NC" in folder name

        folders = os.listdir(path)
        for folder in folders:
            if 'NC' not in folder:
                elt = Magefin(self.model_name)
                elt.path = path + '/' + folder
                # print('MY PATH: ', elt.path)
                elt.Read_one(option=option)
                self.folders[folder] = elt

    def Read_exported_Z(self,bankful):
        """
                to read the Z files exported in different Pk pts using *.REP file
                :param path: path of the exported files
                :return: dictionary of different Pk positions and Z(t) in those PK
                """
        par_xi = [(elt.split('/')[-1].split('.')[0].split('_')[1], int(elt.split('/')[-1].split('.')[0].split('_')[-1]))
                  for elt in glob.glob(self.path + '/*.csv')]

        # we gather the files of each parameter in a list. Only have z here:
        files_z = [elt for elt in par_xi if elt[0] == 'z']
        pathi = [elt for elt in glob.glob(self.path + '/*.csv') if elt.split('/')[-1].split('.')[0].split('_')[1] == 'z']
        nms = [['t(s)', 'Z(x=' + str(i[1]) + 'm)'] for i in files_z]
        # files reading
        Zi = {j[1]: pd.read_csv(elt, header=None, encoding="ISO-8859-1", sep="\s+", skiprows=1, names=i) for (j, elt, i)
              in zip(files_z, pathi, nms)}
        # bed_depths = pd.read_csv(self.path + '/Mage_fin.ini', header=None, encoding="ISO-8859-1", sep="\s+", skiprows=9,
        #                          usecols=[4, 5], names=['Pm', 'fond'])

        bed_depths = self.Steady[['Pm(m)','Z_fond']]

        # Largeurs au miroirs
        Bf, Bm = 1000, 1000
        B = (Bf + Bm)
        # converting time to sec & depths to mm and adding the Hmoy: mean water depths
        for (elt, i) in zip(Zi.keys(), files_z):
            Zi[elt].iloc[:, 0] = Zi[elt].iloc[:, 0] * 3600
            Zi[elt].iloc[:, 1] = Zi[elt].iloc[:, 1] * 1000
            #     hauteurs d'eau
            Zi[elt]['Hm(x=' + str(i[1]) + 'm)'] = Zi[elt].iloc[:, 1] - float(
                bed_depths['Z_fond'].loc[bed_depths['Pm(m)'] == i[1]]) * 1000
            Zi[elt]['Hf(x=' + str(i[1]) + 'm)'] = Zi[elt]['Hm(x=' + str(i[1]) + 'm)'] - bankful * 1000
            S_mouille = Zi[elt]['Hm(x=' + str(i[1]) + 'm)'] * Bm + Zi[elt]['Hf(x=' + str(i[1]) + 'm)'] * Bf
            Zi[elt]['Hmoy(x=' + str(i[1]) + 'm)'] = S_mouille / B

            Rh = {}
            Rh['MC'] = Zi[elt]['Hm(x=' + str(i[1]) + 'm)'] * Bm / (
                        Zi[elt]['Hm(x=' + str(i[1]) + 'm)'] + Bm + bankful * 1000)
            Rh['FP'] = Zi[elt]['Hf(x=' + str(i[1]) + 'm)'] * Bf / (Zi[elt]['Hf(x=' + str(i[1]) + 'm)'] + Bf)
            self.Rh_DT[elt] = Rh

        self.Z_DT = Zi

    def Read_exported_Q(self):
        """
        To read the exported hydrographs Qtotal(t), Qmainchannel(t), Qfloodplain(t) using *.REP File
        :param path: path where the files are exported
        :return:tuple of pandas of Q(L/s) as function of time (s)
        """

        par_xi = [(elt.split('/')[-1].split('.')[0].split('_')[1], int(elt.split('/')[-1].split('.')[0].split('_')[-1]))
                  for elt in glob.glob(self.path + '/*.csv')]
        #       we gather the files of each parameter in a list. have qm & qr here:
        path_qm, path_qr = [elt for elt in glob.glob(self.path + '/*.csv') if
                            elt.split('/')[-1].split('.')[0].split('_')[1] == 'qm'], [elt for elt in
                                                                                      glob.glob(self.path + '/*.csv') if
                                                                                      elt.split('/')[-1].split('.')[
                                                                                          0].split('_')[1] == 'ql']

        files_qm, files_qr = [elt for elt in par_xi if elt[0] == 'qm'], [elt for elt in par_xi if elt[0] == 'ql']
        nms_qm, nms_qr = [['t(s)', 'Qm(x=' + str(i[1]) + 'm)'] for i in files_qm], [['t(s)', 'Ql(x=' + str(i[1]) + 'm)']
                                                                                    for i in files_qr]
        #         files reading
        Qmi = {j[1]: pd.read_csv(elt, header=None, encoding="ISO-8859-1", sep="\s+", skiprows=1, names=i) for
               (j, elt, i) in zip(files_qm, path_qm, nms_qm)}
        Qri = {j[1]: pd.read_csv(elt, header=None, encoding="ISO-8859-1", sep="\s+", skiprows=1, names=i) for
               (j, elt, i) in zip(files_qr, path_qr, nms_qr)}
        for (qm, m) in zip(Qmi.keys(), files_qm):
            Qmi[qm].iloc[:, 0] = Qmi[qm].iloc[:, 0] * 3600
            Qmi[qm].iloc[:, 1] = Qmi[qm].iloc[:, 1] * 1000
            #           vels (cm/s), considering that the width Bm=Br=1
            Qmi[qm]['U(x=' + str(m[1]) + 'm)'] = Qmi[qm].iloc[:, 1] / self.Z_DT[qm].iloc[:, 2]
        for (qr, r) in zip(Qri.keys(), files_qr):
            Qri[qr].iloc[:, 0] = Qri[qr].iloc[:, 0] * 3600
            Qri[qr].iloc[:, 1] = Qri[qr].iloc[:, 1] * 1000
            #           vels (cm/s), considering that the width Bm=Br=1
            Qri[qr]['U(x=' + str(r[1]) + 'm)'] = Qri[qr].iloc[:, 1] / self.Z_DT[qr].iloc[:, 3]

        self.Q_DT = (Qmi, Qri)

    def get_psi(self,path):
        """
        read psi from result_ISM_Uniform.txt
        :param path: path of magefin/file.txt
        :return:
        """
        with open(path) as f:
            data = f.readlines()

        flag = False
        for elt in data:
            if 'Solution' in elt:
                flag = True
                pass
            else:
                if flag:
                    psi = elt
                    return float(psi.split('\n')[0])

    def get_Sfi(self,option='I'):
        self.Steady['Sff'] = (1/self.Steady['Kf']) ** 2 * self.Steady['Vf'] ** 2 / self.Steady['Rh_f'] ** (4 / 3)
        self.Steady['Sfm'] = (1/self.Steady['Km']) ** 2 * self.Steady['Vm'] ** 2 / self.Steady['Rh_m'] ** (4 / 3)


    def compute_psi(self):
        Am, Af = self.Steady['Qm']/self.Steady['Vm'], self.Steady['Qf']/self.Steady['Vf']
        denom = 2 / Am + 1 / Af

        self.Steady['psi']= g * (self.Steady['Sff'] - self.Steady['Sfm']) /(self.Steady['Hf'] * (self.Steady['Vm'] - self.Steady['Vf']) ** 2) / denom


    def get_lambda(self):
        return np.mean((self.Steady['Vm'] - self.Steady['Vf'])/(self.Steady['Vm'] + self.Steady['Vf']))

    def get_last_time(self,path):

        P = path+'/Mage_fin.ini'

        with open(P,'r') as file:
            file.readlines()
        with open(P,'r') as file:
            data = [x.strip() for x in file]
        time_ = datetime.strptime(data[1][52:60], '%H:%M:%S')
        self.last_time_step = round(time_.hour+ time_.minute/60 + time_.second/3600,4)


    def get_parameter_RESfile(self,extraire_name,files_names,parameter, nb_bief, path):
        """
        get parameter from RES Files (many files when we have many branches)
        :param extraire_name: mage_extraire or the one defined (character)
        :param model_name: (character)
        :param files_names: output names (list of characters)
        :param parameter: (character)
        :param nb_bief: (list of characters)
        :param path:
        :return:
        """

        self.get_last_time(path)

        os.chdir(path)
        for nb,file in zip(nb_bief,files_names):
            command = extraire_name+' '+self.model_name + ' ' + parameter + ' ' + nb+ ' h'+ str(self.last_time_step)
            os.system(command)
            os.system('mv '+self.model_name+'.res '+file+'.res')
            print('Command pour RES : ', command )

        data = [pd.read_csv(path+'/'+file+'.res', header=None,encoding="UTF-8", sep="\s+", skiprows=3,names=['Pk',parameter]) for file in files_names]
        self.q_values = pd.concat(data, ignore_index=True)


    def correct_Vint(self):
        """
        corrige les valeurs Uint en aval du bief (=0 dans le calcul)
        :return:
        """

        path = self.path+'/'+self.model_name+'.TRA'
        with open(path) as f:
            data = f.readlines()

        indx=[i for i, elt in enumerate(data) if '-w' in elt][0]
        self.weight=float(data[indx].split('-w=')[1][:3])

        self.Steady['Uint_corrected'] = self.weight*self.Steady['Vm']+(1-self.weight)*self.Steady['Vf']

    def compute_q(self):
        """
        Calcul le transfert de masse q en permanent : q = dQf/dx
        :return:
        """

        DQf = np.array(self.Steady['Qf'].iloc[1:]) - np.array(self.Steady['Qf'].iloc[:-1])
        Dx = np.array(self.Steady['Pm(m)'].iloc[1:]) - np.array(self.Steady['Pm(m)'].iloc[:-1])

        #suppose we have only one Dx=0 (one junction)
        index_nan = [ind for ind,elt in enumerate(Dx) if elt==0]

        if len(index_nan)>0:
            ind= index_nan[0]
            DQf[ind]=DQf[ind-1]
            Dx[ind]=Dx[ind-1]

        new_q = list(DQf/Dx) + [list(DQf/Dx)[-1]]
        self.Steady['q_computed'] = new_q