import os
import numpy as np
from Mage.utils import check_object
from Mage.Geometry.section import section
from Mage.Geometry.bief import bief

os.chdir('/home/yassine.kaddi/Scripts/Untitled Folder/Methodo/Script_CNR/Crue10_tools-master')
from crue10.etude import Etude
from crue10.modele import Modele
from crue10.emh.section import SectionProfil
from crue10.emh.branche import BrancheSaintVenant


# To modify:
#     Branch id = 1

class Transfert_Branch_CRUE10(bief):
    def __init__(self):
        self.etude_CRUE10 = Etude
        self.etude_model = Modele
        self.branches = []

    def read(self, path1, xml_name, modele_name):
        # example xml_name: 'Etu_BE2016_conc.etu.xml'
        # example path1 :'/home/yassine.kaddi/usb/cnr/etude_halimatou/BE'
        # example modele_name : 'Mo_BE2016_Halimatou1
        self.etude_CRUE10 = Etude(os.path.join(path1 + '/', xml_name))
        self.etude_model = self.etude_CRUE10.get_modele(modele_name)
        self.etude_model.read_all()

    def branch_to_mage(self, branch):
        if isinstance(branch, BrancheSaintVenant):
            #             Filter SectionIdem
            sections_crue10 = [sect for sect in branch.liste_sections_dans_branche if check_object(sect, SectionProfil)]

            sections_mage = [self.convert_section(sect) for sect in sections_crue10]
            self.node_upstream = branch.noeud_amont.id
            self.node_downstream = branch.noeud_aval.id
            self.id = 1
            self.sections = sections_mage
        else:
            print('{} is supposed to be type {}'.format(branch, BrancheSaintVenant))

    def branches_to_mage(self, branch):
        if isinstance(branch, list):
            listes_sections= [br.liste_sections_dans_branche for br in branch]

            # Correction des xp
            last = 0
            for many_sections in listes_sections:
                for sect in many_sections:
                    sect.xp = sect.xp + last
                last = sect.xp

            listes_sections_flattened = [elt for sublist in listes_sections for elt in sublist]

            #             Filter SectionIdem
            sections_crue10 = [sect for sect in listes_sections_flattened if check_object(sect, SectionProfil)]

            sections_mage = [self.convert_section(sect) for sect in sections_crue10]
            self.node_upstream = branch[0].noeud_amont.id
            self.node_downstream = branch[-1].noeud_aval.id
            self.id = 1
            self.sections = sections_mage
        else:
            print('{} is supposed to be type {}'.format(branch, list))

    def get_all_cnr_branches(self):
        try:
            self.branches = self.etude_model.get_liste_branches()
        except:
            print('Problem with your model')

    def convert_section(self, section_cnr):
        section0 = section()
        nb_pts = len(section_cnr.xz)

        section0.x = np.array([section_cnr.xp] * nb_pts)
        section0.y = np.array([elt[0] for elt in section_cnr.xz])
        section0.z = np.array([elt[1] for elt in section_cnr.xz])
        noms = self.transfert_section_caracteristiques(section_cnr)
        section0.Nom = noms['interface']
        section0.Nom_section = noms['nom_section']
        section0.pk = section_cnr.xp
        return section0

    def transfert_section_caracteristiques(self, section_cnr):
        Yi = np.array([elt[0] for elt in section_cnr.xz])
        nom_section = section_cnr.id
        Mydict = {yi: '' for yi in Yi}
        RD = [(elt.xt_min, elt.xt_max) for elt in section_cnr.lits_numerotes if 'MajD' in elt.id][0]
        RG = [(elt.xt_min, elt.xt_max) for elt in section_cnr.lits_numerotes if 'MajG' in elt.id][0]

        # Existence de lit majD
        if RD[0] != RD[1]:
            Mydict[RD[1]] = 'RD'
        else:
            Mydict[RD[1]] = ''

        if RG[0] != RG[1]:
            Mydict[RG[0]] = 'RG'
        else:
            Mydict[RG[0]] = ''

        Noms = [Mydict[elt] for elt in Mydict]
        result = {'interface':Noms,
                  'nom_section':nom_section}
        return result