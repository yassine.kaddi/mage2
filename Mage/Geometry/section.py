import numpy as np

class section:

    def __init__(self):
        TYPES = ['ProfilXYZ', 'ProfilAC', 'ProfilLC', 'ProfilR', 'ProfilT', 'ProfilRT']
        #         self.nb_pts = 0
        self.pk = 0
        self.Nom = []
        self.Nom_section = ''
        self.slope = 0
        self.type = TYPES[0]
        self.indice=1
        self.x = np.array([])
        self.y = np.array([])
        self.z = np.array([])

    #         self.data = {'x':self.x,'y':self.y,'z':self.z,'nom':[],'Commentaire':[],'abs':[]}

    def compute_section(self, section2, pos='upstream'):
        # When the cross section doesn't change from section2, situated upstream or downstream section2
        print("Don't forget to update pk, X and K when section is computed")
        self.pk = section2.pk + 1
        self.slope = section2.slope
        self.type = section2.type
        self.y = section2.y
        self.Nom = section2.Nom
        try:
            if pos == 'upstream':
                self.z = section2.z + self.slope * np.abs(section2.x - self.x)

            if pos == 'downstream':
                self.z = section2.data['z'] - self.slope * np.abs(section2.x - self.x)
        except:
            print('Problem in Z computation')
