from Mage.Files.file_ST import file_ST
import os
import subprocess

class bief:
    def __init__(self):
        self.sections = []
        self.node_upstream = ''
        self.node_downstream = ''
        self.rug = []  # [[(xmin,xmax),(Kmin,Kmoy)],]
        self.psi = []  # [[(xmin,xmax),psi],]
        self.id = 1
        self.pk_lim = ()
        self.etude = ''
        self.hyd = 0
        self.hyd_time=None
        self.lim = 0
        self.path = ''

    def export_ST(self, path):
        if len(self.sections) == 0:
            print('No cross-sections')
            return

        ST_file = file_ST()
        ST_file.name = 'Bief_' + str(self.id) + '.ST'
        self.path = path
        path_ST = path + '/' + ST_file.name
        with open(path_ST, 'w') as file:
            [file.write(ST_file.write_file(sect)) for sect in self.sections]

    def get_pk_lim(self):
        if len(self.sections) == 0:
            print('No cross-sections')
            return

        self.pk_lim = (min([sec.pk for sec in self.sections]), max([sec.pk for sec in self.sections]))

    def run_mesh(self,resolution):
        os.chdir(self.path+'/MailleurPF/')
        p = subprocess.Popen(['mesh','Bief_1.ST'],stdin=subprocess.PIPE,stdout=subprocess.PIPE)
        p.communicate(input='{}'.format(resolution).encode())