class file_RUG:
    def __init__(self):
        self.name = []

    def write_file(self, bief):
        if len(bief) == 0:
            print('Biefs are empty!')
            return

        lines = []
        header = '*Bief        x deb     x fin     K min     K moy \n*---======----------==========----------==========\n'

        lines.append(header)
        for elt in range(len(bief)):
            for pki, Ki in bief[elt].rug:
                line = 'K  ' + str(bief[elt].id) + '       ' + self.write_number(pki[0]) + '      ' + self.write_number(
                    pki[1]) + '      ' + self.write_number(Ki[0]) + '    ' + self.write_number(Ki[1]) + '\n'
                lines.append(line)

        lines = ''.join(lines)
        return lines

    def write_number(self, number):
        # to write a number in RUG format
        max_len = 5
        number_str = str(number)
        while len(number_str) < max_len:
            if '.' in number_str:
                integ, decm = number_str.split('.')
                decm = '.' + decm + '0'
                number_str = integ + decm
            else:
                number_str += '.0000'

        number_str = number_str[:max_len]
        return number_str

    def read_file(self):
        print('Not yet written')