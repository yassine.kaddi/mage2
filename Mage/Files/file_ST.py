import pandas as pd

class file_ST:

    def __init__(self):
        self.sections= []
        self.name = ''

    def write_file(self ,sections):
        # write a section bloc in ST file
        if (len(sections.x) != len(sections.y)) or (len(sections.x) != len(sections.y)):
            print('X, Y and Z have different lengths!')
            return

        nb_pts =str(len(sections.x))
        header= '     ' +str(sections.indice) +'     ' +'0' +'     ' +'0' +'     ' +nb_pts\
                +'       ' +self.write_number(sections.x[0]) +'   '+ sections.Nom_section +' \n'
        lines= []
        lines.append(header)
        for elt in range(len(sections.x)):
            line =  '       ' +self.write_number(sections.x[elt] ) +'       ' +self.write_number \
                (sections.y[elt] ) +'       ' +self.write_number(sections.z[elt] ) +' '+ sections.Nom[elt ] +'\n'
            lines.append(line)
        last_line = '     999.9990     999.9990     999.9990 \n'
        lines.append(last_line)
        lines =''.join(lines)
        return lines

    def write_number(self ,number):
        # to write a number in ST format (4 decim)
        max_len =6
        number_str =str(number)
        while len(number_str) <max_len:
            if '.' in number_str:
                integ ,decm = number_str.split('.')
                decm ='.' + decm + '0'
                number_str = integ + decm
            else:
                number_str += '.0000'

        number_str = number_str[:max_len]
        return number_str

    def get_data_meshing(self,path):
        data = pd.read_csv(path, header=None, encoding = "ISO-8859-1", sep="\s+",skiprows=3, usecols=[0,1,2,3,4])
        data.columns = ['X','Y','Z','Rives','PK']

        data['PK'] = data['PK'].fillna(method='ffill')


        RD = data[['PK','Y','Z']].loc[data['Rives']=='RD'.lower()]
        RG = data[['PK','Y','Z']].loc[data['Rives']=='RG'.lower()]
        Fond = data[['PK','Y','Z']].loc[data['Rives']=='fon']

        return (RD,RG,Fond)

    def read_file(self):
        print('Not yet written')