class file_LIM:
    def __init__(self):
        self.name= ''

    def write_file(self,Z,slug):

        lines = []
        header = '$'+slug+'\n*temps(s) |cotes(m)\n*---------++++++++++\n'
        time = [0,1]
        lines.append(header)
        for t in time:
            line = '   ' + self.write_number(t) + '   ' + self.write_number(Z) + '\n'
            lines.append(line)

        lines = ''.join(lines)

        return lines

    def write_number(self, number):
        # to write a number in HYD format
        max_len = 5
        number_str = str(number)
        while len(number_str) < max_len:
            if '.' in number_str:
                integ, decm = number_str.split('.')
                decm = '.' + decm + '0'
                number_str = integ + decm
            else:
                number_str += '.00'

        number_str = number_str[:max_len]
        return number_str

    def read_file(self):
        print('Not yet written')