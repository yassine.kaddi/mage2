from Mage.Files.file_M import file_M

class file_INI:
    def __init__(self):
        self.name= ''
        self.bief_nb = 1
        self.fond_indicator = 'FD'
        self.depth=1.
        self.discharge = 1.
        self.path = ''

    def export_INI(self,option='HC'):
        mesh = file_M()
        mesh.file_name='Bief_'+str(self.bief_nb)
        mesh.path = self.path

        mesh.read_file()

        path = self.path+'/'+self.name
        with open(path,'w') as f:
            writing = mesh.write_file(self.bief_nb,self.discharge, self.fond_indicator, self.depth,option)
            f.write(writing)
