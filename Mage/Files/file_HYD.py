class file_HYD:
    def __init__(self):
        self.name= ''

    def write_file(self, inlet,slug,time=None):

        if isinstance(inlet,tuple) or isinstance(inlet,list):
            lines = self.write_file_distribution(inlet, slug,time)
        else:
            lines = self.write_file_total(inlet,slug,time)


        return lines


    def write_file_distribution(self,inlet,slug,time):

        lines = []
        header = '$'+slug+'\n*temps(mn)|Débit(m3/s)\n*---------++++++++++----------++++++++++\n'
        if time is None:
            time = [0,1]

        lines.append(header)
        if isinstance(inlet,list):
            for t,inl in zip(time,inlet):
                line = '  ' + self.write_number(t) + '    ' + self.write_number(inl[0]) + '     '+ self.write_number(inl[1]) + '     '+ self.write_number(inl[2]) + '\n'
                lines.append(line)
        else:
            for t in time:
                line = '  ' + self.write_number(t) + '    ' + self.write_number(inlet[0]) + '     '+ self.write_number(inlet[1]) + '     '+ self.write_number(inlet[2]) + '\n'
                lines.append(line)

        lines = ''.join(lines)
        return lines

    def write_file_total(self,inlet,slug,time):

        lines = []
        header = '$'+slug+'\n*temps(mn)|Débit(m3/s)\n*---------++++++++++----------++++++++++\n'
        if time is None:
            time = [0,1]

        lines.append(header)
        if isinstance(inlet,list):
            for t,inl in zip(time,inlet):
                line = '  ' + self.write_number(t) + '    ' + self.write_number(inl) + '\n'
                lines.append(line)
        else:
            for t in time:
                line = '  ' + self.write_number(t) + '    ' + self.write_number(inlet) + '\n'
                lines.append(line)

        lines = ''.join(lines)
        return lines

    def write_number(self, number):
        # to write a number in HYD format
        max_len = 6
        number_str = str(number)
        while len(number_str) < max_len:
            if '.' in number_str:
                integ, decm = number_str.split('.')
                decm = '.' + decm + '0'
                number_str = integ + decm
            else:
                number_str += '.00'

        number_str = number_str[:max_len]
        return number_str

    def read_file(self):
        print('Not yet written')