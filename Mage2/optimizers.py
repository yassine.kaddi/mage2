from Mage2.utils import round_time, convert_str_hours

import os
import subprocess
import time
import signal
from tqdm.notebook import tqdm
from hyperopt import hp, fmin, tpe, Trials



class optimizers:
    """
    class for optimizing Etude
    """

    def __init__(self,etude):
        self.etude = etude
        self.bayes_file = 'bayes_pars.txt'
        self.all_file = 'all_pars.txt'
        self.bayes_space = {} #in shape of space of hyperopt package
        self.sleep = 2  # time to wait in each simulation

        header = 'param_implic   tmax   tmin   coeff_liss   reduc_pdt   reduc_ZQ   psi   mesh   w   INI_depth   is_permanent   loss \n'
        with open(self.etude.path + '/' + self.bayes_file, 'a') as f:
            f.write(header)

    def run_sim(self, w, i=1):
        """
        run etude
        :param etude:
        :param w: weight in ISM simulation
        :param i:
        :return: loss
        """

        time_expected = self.etude.NUM.params['final_time_hour']
        # Run
        os.chdir(self.etude.path)

        p = subprocess.Popen(['{} -w={} -i={} {}'.format(self.etude.mage_command, round(w, 2), i, self.etude.name)],
                             stdin=subprocess.PIPE, stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
        time.sleep(self.sleep)  # sleep time is evaluated based on how much 1 simulation is supposed to last

        #  check if simulation started
        if self.etude.output.simulation_stopped():
            os.killpg(p.pid, signal.SIGTERM)
            return 1

        # calculate obj function
        self.etude.output.read_file()
        time_simulated = self.etude.output.params['time_sim']

        os.killpg(p.pid, signal.SIGTERM)

        return (time_expected - convert_str_hours(time_simulated)) / time_expected


    def transform_opt_results(self,trials):
        loss = []

        keys = list(trials.__dict__['_dynamic_trials'][0]['misc']['vals'].keys())
        vals = {}
        for key in keys:
            vals[key] = []

        for i in range(len(trials) - 1):
            loss.append(trials.__dict__['_dynamic_trials'][i]['result']['loss'])
            dic = trials.__dict__['_dynamic_trials'][i]
            for key in keys:
                vals[key].append(dic['misc']['vals'][key][0])
        return {'loss': loss, 'vals': vals}



    def bayes_obj_func(self,space):

        """
        compute the objectif function for bayesian optimizer to succeed a simulation / done for only 1 bief
        :return:  (time of simulation - expected final time)/expected final time
        """
        args = []
        self.etude.biefs.read_biefs()
        if len(space) >0 :
            # Export
                # NUM values
            self.etude.NUM.read_file()
            if 'param_implic' in space:
                self.etude.NUM.params['param_implic'] = round(space['param_implic'], 2)
                args.append('param_implic')

            if 'tmax' in space:
                self.etude.NUM.params['tmax'] = round(space['tmax'], 2)
                args.append('tmax')

            if 'tmin' in space:
                self.etude.NUM.params['tmin'] = round(space['tmin'], 3)
                args.append('tmin')

            if 'coeff_liss' in space:
                self.etude.NUM.params['coeff_liss'] = round(space['coeff_liss'], 5)
                args.append('coeff_liss')

            if 'reduc_pdt' in space:
                self.etude.NUM.params['reduc_pdt'] = space['reduc_pdt']
                args.append('reduc_pdt')

            if 'reduc_ZQ' in space:
                self.etude.NUM.params['reduc_ZQ'] = space['reduc_ZQ']
                args.append('reduc_ZQ')

            if 'is_permanent' in space:
                self.etude.NUM.export_NUM(space['is_permanent'])
                args.append('is_permanent')
            #
            if 'mesh1' in space:
                # self.etude.biefs.read_biefs()
                self.etude.biefs.elements[0].run_mesh(space['mesh1'])
                args.append('mesh1')

            if 'mesh2' in space:
                # self.etude.biefs.read_biefs()
                self.etude.biefs.elements[1].run_mesh(space['mesh2'])
                args.append('mesh2')

            if 'mesh3' in space:
                # self.etude.biefs.read_biefs()
                self.etude.biefs.elements[2].run_mesh(space['mesh3'])
                args.append('mesh3')

            if 'mesh4' in space:
                # self.etude.biefs.read_biefs()
                self.etude.biefs.elements[3].run_mesh(space['mesh4'])
                args.append('mesh4')

            if 'rives1' in space:
                self.etude.biefs.elements[0].read_bief_sections()
                all_sects = self.etude.biefs.elements[0].sections.elements
                for pk in all_sects:
                    all_sects[pk].move_Rives(space['rives1'])
                self.etude.biefs.elements[0].run_mesh(space['mesh1'])
                self.etude.biefs.elements[0].export_ST(12, verbose=False)

            if 'rives2' in space:
                self.etude.biefs.elements[1].read_bief_sections()
                all_sects = self.etude.biefs.elements[1].sections.elements
                for pk in all_sects:
                    all_sects[pk].move_Rives(space['rives2'])
                self.etude.biefs.elements[1].run_mesh(space['mesh2'])
                self.etude.biefs.elements[1].export_ST(12, verbose=False)

            if 'rives3' in space:
                self.etude.biefs.elements[2].read_bief_sections()
                all_sects = self.etude.biefs.elements[2].sections.elements
                for pk in all_sects:
                    all_sects[pk].move_Rives(space['rives3'])
                self.etude.biefs.elements[2].run_mesh(space['mesh3'])
                self.etude.biefs.elements[2].export_ST(12, verbose=False)

            if 'rives4' in space:
                self.etude.biefs.elements[3].read_bief_sections()
                all_sects = self.etude.biefs.elements[3].sections.elements
                for pk in all_sects:
                    all_sects[pk].move_Rives(space['rives4'])
                self.etude.biefs.elements[3].run_mesh(space['mesh4'])
                self.etude.biefs.elements[3].export_ST(12, verbose=False)

                # PSI
            if 'psi' in space:
                self.etude.biefs.read_biefs()
                X0,Xf = self.etude.biefs.elements[0].pk_lim
                self.etude.biefs.elements[0].psi = [[(X0, Xf), round(space['psi'], 4)], ]
                self.etude.export_PSI()
                args.append('psi')

                    # INI
            if 'INI_depth' in space:
                fond_indic = 'fon'
                self.etude.HYD.read_file()
                option=space['INI_opt']
                # discharge = self.HYD.discharge
                discharge = 250
                INI_args = (round(space['INI_depth'],2), fond_indic,discharge,option)
                self.etude.biefs.elements[0].export_INI(*INI_args)
                args.append('INI_depth')
            else:
                space['INI_depth'] = ''

            loss = self.run_sim(space['w'])
            sep=';'
            with open(self.etude.path + '/' + self.bayes_file, 'a') as f:
                line = ''
                for elt in args:
                    line += str(space[elt]) + sep
                    f.write(line+sep+str(loss)+'\n')
                # f.write(str(space['param_implic']) + sep + str(space['tmax']) + sep + str(space['tmin']) + sep + str(
                #     space['coeff_liss']) + sep + str(space['reduc_pdt']) + sep +
                #         str(space['reduc_ZQ']) + sep + str(space['psi']) + sep + str(space['mesh']) + sep + str(
                #     space['w']) + sep + str(space['INI_depth']) + sep +
                #         str(space['is_permanent']) + sep + str(loss) + '\n')
                # f.write(str(space['param_implic']) + sep + str(space['tmax']) + sep + str(space['tmin']) + sep + str(
                #     space['coeff_liss']) + sep + str(space['reduc_pdt']) + sep +
                #         str(space['reduc_ZQ']) + sep + str(
                #     space['w']) + sep + str(space['INI_depth']) + sep +
                #         str(space['is_permanent']) + sep + str(loss) + '\n')

            return loss
        else:
            print('no bayes_space found')
            return


    def bayes_optimizer(self, max_evals):
        """
        optimize parameters defined in self.space using bayes optimizer based on hyperopt package
        :param etude: etude
        :param max_evals: number of runs
        :return:
        """
        self.etude.LIM.read_file()
        cote = self.etude.LIM.cote
        sigma = 1

        # # BV Etude
        # space = {'param_implic': hp.choice('param_implic', list(np.arange(.65,.71,step=.01))),
        #          'tmax': hp.randint('tmax', 70, 150),
        #          'tmin': hp.uniform('tmin', .001, .01),
        #          'coeff_liss': hp.choice('coeff_liss', [0,.0001,.001,.01,.1]),
        #          'reduc_pdt': hp.choice('reduc_pdt', [1, 2, 3, 4, 5]),
        #          'reduc_ZQ': hp.choice('reduc_ZQ', [(1, 1), (2, 2), (3, 3), (4, 4),(5,5)]),
        #          'mesh': hp.choice('mesh',list(np.arange(70,121,step=10))),
        #          'psi': hp.uniform('psi', 0.01, 0.1),
        #          'w': hp.choice('w',[0.2,0.3,0.4,0.5,0.6]),
        #          # 'INI_depth': hp.normal('INI_depth', cote, sigma)
        #           }

        # CM Etude
        # space = {'param_implic': hp.normal('param_implic', .7,sigma),
        #          'tmax': hp.randint('tmax', 1, 10),
        #          'tmin': hp.uniform('tmin', .05, 1.),
        #          'coeff_liss': hp.choice('coeff_liss', [0,.0001]),
        #          'reduc_pdt': hp.choice('reduc_pdt', [1, 2, 3, 4, 5]),
        #          'reduc_ZQ': hp.choice('reduc_ZQ', [(1, 1), (2, 2), (3, 3), (4, 4),(5,5)]),
        #          'mesh': hp.choice('mesh',list(np.arange(.1,.51,step=.1))),
        #          'psi': hp.uniform('psi', 0.01, 0.1),
        #          'w': hp.choice('w',[0.2,0.3,0.4,0.5,0.6]),
        #          # 'INI_depth': hp.normal('INI_depth', cote, sigma)
        #           }
        trials = Trials()

        best = fmin(fn=self.bayes_obj_func,
                    space = self.bayes_space,
                    algo = tpe.suggest,
                    max_evals=max_evals,
                    trials=trials)
        res = self.transform_opt_results(trials)
        return best,res



    def run_all(self,space):
        """
        run etude with all combinations of space
        :param etude: etude
        :param space: parameters to be considered
        :return:
        """
        self.etude.LIM.read_file()
        cote = self.etude.LIM.cote

        # CM etude
        # space = {'param_implic': [.68,.7],
        #          'tmax': np.arange(9,10),
        #          'tmin': np.arange(0,1.,step=.5),
        #          'coeff_liss': [0,.0001],
        #          'reduc_pdt': [2, 3],
        #          'reduc_ZQ':[ (2, 2)],
        #          'mesh':[.1],
        #          'psi': [10,.02],
        #          'w': [.2,.3],
        #          # 'INI_depth': hp.normal('INI_depth', cote, sigma)
        #           }

        loss = []
        vals = []

        for param_implic in space['param_implic']:
            for tmax in space['tmax']:
                for tmin in space['tmin']:
                    for coeff_liss in space['coeff_liss']:
                        for reduc_pdt in space['reduc_pdt']:
                            for reduc_ZQ in space['reduc_ZQ']:
                                for mesh in space['mesh']:
                                    for psi in space['psi']:
                                        for w in space['w']:
                                            args = (param_implic, tmax, tmin, coeff_liss, reduc_pdt, reduc_ZQ, mesh, psi, w,space['is_permanent'])
                                            # print(args)
                                            vals.append(args)
        for arg in tqdm(vals):
            loss.append(self.run_all_obj_func(arg))

        return (loss,vals)




    def run_all_obj_func(self, args):

        """
        compute the objectif function for run_sims  / done for only 1 bief
        :param etude: etude
        :param args: param_implic,tmax,tmin,coeff_liss,reduc_pdt,reduc_ZQ,mesh,psi (psi format, see bief.py),w,INI (depth,fond_indic,discharge,option)
        :return:  time of simulation - expected final time (in hours)
        """

        param_implic, tmax, tmin, coeff_liss, reduc_pdt, reduc_ZQ, mesh, psi, w,is_permanent = args

        # Export
        # NUM values
        self.etude.NUM.read_file()
        self.etude.NUM.params['param_implic'] = round(param_implic, 2)
        self.etude.NUM.params['tmax'] = round(tmax, 2)
        self.etude.NUM.params['tmin'] = round(tmin, 3)
        self.etude.NUM.params['coeff_liss'] = round(coeff_liss, 5)
        self.etude.NUM.params['reduc_pdt'] = reduc_pdt
        self.etude.NUM.params['reduc_ZQ'] = reduc_ZQ

        self.etude.NUM.export_NUM(is_permanent)

        # mesh

        self.etude.biefs.read_biefs()
        self.etude.biefs.elements[0].run_mesh(mesh)

        # PSI
        self.etude.biefs.read_biefs()
        X0, Xf = self.etude.biefs.elements[0].pk_lim
        self.etude.biefs.elements[0].psi = [[(X0, Xf), round(psi, 4)], ]
        self.etude.export_PSI()

        # INI
        # fond_indic = 'fon'
        # self.HYD.read_file()
        # option='CC'
        # discharge = self.HYD.discharge
        # INI_args = (round(space['INI_depth'],2), fond_indic,discharge,option)
        # self.biefs.elements[0].export_INI(*INI_args)

        loss = self.run_sim(w)

        return loss