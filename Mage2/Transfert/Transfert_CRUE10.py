import os
import numpy as np
import pandas as pd
from Mage2.utils import check_object
from Mage2.Geometry.section import sections, section
from Mage2.Geometry.bief import biefs, bief

os.chdir('/home/yassine.kaddi/Scripts/MyNotebooks/Methodo/Script_CNR/Crue10_tools-master')
from crue10.etude import Etude
from crue10.modele import Modele
from crue10.emh.section import SectionProfil


# To modify:
#     Branch id = 1


class Transfert_Geometry_CRUE10():
    def __init__(self, name='',path='',args=('rg','rd', 'fon')):
        self.path = path
        self.name = name
        self.args = args
        self.etude_CRUE10 = Etude
        self.etude_model = Modele
        self.biefs_CRUE = []


    def read(self, path1, xml_name, modele_name):
        # example xml_name: 'Etu_BE2016_conc.etu.xml'
        # example path1 :'/home/yassine.kaddi/usb/cnr/etude_halimatou/BE'
        # example modele_name : 'Mo_BE2016_Halimatou1
        self.etude_CRUE10 = Etude(os.path.join(path1 + '/', xml_name))
        self.etude_model = self.etude_CRUE10.get_modele(modele_name)
        self.etude_model.read_all()

    def get_all_cnr_branches(self):
        try:
            self.biefs_CRUE = self.etude_model.get_liste_branches()
        except:
            print('Problem with your model')


    def convert_biefs(self, liste_biefs_CRUE):
        biefs_mage =  biefs(self.name,self.path, self.args)
        if isinstance(liste_biefs_CRUE, list):
            for ind,bief_crue in enumerate(liste_biefs_CRUE):
                biefs_mage.elements[ind] = self.convert_bief(bief_crue,ind+1)

        else:
            print('{} is supposed to be type {}'.format(liste_biefs_CRUE, list))
        return biefs_mage

    def filter_SectionIdem(self, bief_CNR):
        sections_ = bief_CNR.liste_sections_dans_branche
        filtered_sections = [sect for sect in sections_ if check_object(sect, SectionProfil)]
        return filtered_sections

    def transfert_section_caracteristiques(self, section_cnr):
        Yi = np.array([elt[0] for elt in section_cnr.xz])
        # nom_section = section_cnr.id
        Mydict = {yi: '' for yi in Yi}
        RD = [(elt.xt_min, elt.xt_max) for elt in section_cnr.lits_numerotes if 'MajD' in elt.id][0]
        RG = [(elt.xt_min, elt.xt_max) for elt in section_cnr.lits_numerotes if 'MajG' in elt.id][0]

        # Existence de lit majD
        if RD[0] != RD[1]:
            Mydict[RD[1]] = self.args[1]
        else:
            Mydict[RD[1]] = ''

        if RG[0] != RG[1]:
            Mydict[RG[0]] = self.args[0]
        else:
            Mydict[RG[0]] = ''

        Noms = [Mydict[elt] for elt in Mydict]

        return Noms

    def convert_section(self, section_cnr,id = 1):

        # create section_mage
        section_mage = section(self.name, self.path)
        section_mage.pk = round(section_cnr.xp,2)

        nb_pts = len(section_cnr.xz)
        X_data = np.array([section_cnr.xp] * nb_pts)
        Y_data = np.array([elt[0] for elt in section_cnr.xz])
        Z_data = np.array([elt[1] for elt in section_cnr.xz])
        limites_data = self.transfert_section_caracteristiques(section_cnr)

        section_mage.data = pd.DataFrame({'X': [elt for elt in X_data],
                                 'Y': [elt for elt in Y_data],
                                 'Z': [elt for elt in Z_data],
                                 'com': [elt for elt in limites_data]})

        section_mage.data['abs'] = section_mage.data['Y']
        section_mage.indice = id
        section_mage.Nom_section = section_cnr.id

        return section_mage


    def convert_bief(self,bief_CNR,id=1):
        # filter sections
        sections_crue = self.filter_SectionIdem(bief_CNR)

        # convert sections
        indices = np.arange(1,len(sections_crue)+1)
        sections_mage = sections(self.name, self.path,self.args)
        sections_mage.elements = {round(sec.xp,2): self.convert_section(sec, id) for sec, id in zip(sections_crue,indices)}

        # create bief
        new_bief = bief(self.name, self.path, self.args)
        new_bief.node_upstream = bief_CNR.noeud_amont.id
        new_bief.node_downstream = bief_CNR.noeud_aval.id
        new_bief.sections = sections_mage
        new_bief.id = id

        return new_bief
