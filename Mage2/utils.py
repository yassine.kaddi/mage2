# from Mage2.Geometry.bief import bief
# from Mage2.Etude import Model
from datetime import datetime
import pandas as pd
from scipy.interpolate import interp1d
from scipy.integrate import trapz

def to(pd):
    """
    To remove the initial delay
    :param pd:
    :return:
    """
    return pd.loc[pd.iloc[:,0]>=0].reset_index(drop=True)


def check_object(obj,type):
    if not isinstance(obj,type):
        print('{} is supposed to be type {}'.format(obj,type))
        return False
    return True


def get_Sfi(args):
    Bm, Bf, nm, nf, hm, hbk, Qf, Qm = args
    hf = hm - hbk
    Af = Bf * hf
    Am = Bm * hm
    Pf = Bf + hf
    Pm = Bm + 2 * hbk
    Rhf = Af / Pf
    Rhm = Am / Pm
    Sff = nf ** 2 * Qf ** 2 / Rhf ** (4 / 3)
    Sfm = nm ** 2 * Qm ** 2 / Rhm ** (4 / 3)
    return Sff, Sfm


def psi_t_pst(args, opt=1):
    Bm, Bf, nm, nf, hm, hbk, Qf, Qm = args
    hf = hm - hbk
    if opt == 1:
        Sff, Sfm = get_Sfi(args)
    else:
        Sff, Sfm = get_Sfi_Q(args)
    Am, Af = Bm * hm, Bf * hf
    Um, Uf = Qm / Am, Qf / Af
    g = 9.81

    denom = 2 / Am + 1 / Af
    psi = g * (Sff - Sfm) / (hf * (Um - Uf) ** 2) / denom
    return psi


def get_Sfi_Q(args):
    Bm, Bf, nm, nf, hm, hbk, Qf, Qm = args
    hf = hm - hbk
    Af = Bf * hf
    Am = Bm * hm
    Pf = Bf + hf
    Pm = Bm + 2 * hbk
    Rhf = Af / Pf
    Rhm = Am / Pm
    Sff = nf ** 2 * Qf ** 2 / Af ** 2 / Rhf ** (4 / 3)
    Sfm = nm ** 2 * Qm ** 2 / Am ** 2 / Rhm ** (4 / 3)
    return Sff, Sfm


def umbalance_V2(total,qfu,alpha):
    Qf=(1+alpha/100)*qfu
    Qm = total-2*Qf
    return (Qm,Qf)



# def umbalance_V3(total,qfu,alpha,time):
#     # To generalize from my experiment to general channel
#     Qf=(1+alpha/100)*qfu
#     Qm = total-Qf
#     return (Qf,Qm,0,time)


# def Bief_CLM(path, Qt, Qfu, alpha,time=None):
#     # on construit le bief
#     Bief1 = bief()
#     Bief1.node_upstream = 'aaa'
#     Bief1.id = 1
#     Bief1.hyd = umbalance_V2(Qt, Qfu, alpha)
#     # Bief1.hyd = umbalance_V3(Qt, Qfu, alpha,time=None)
#     Bief1.hyd_time=time
#     PN = Model()
#     PN.name = 'CM'
#     PN.bief = [Bief1]
#     PN.export_HYD(path)


def umbalance_unsteady(data, alpha):
    (Qf1, Qm, Qf2) = data
    Qfi1 = [(1 + alpha / 100) * qf for qf in Qf1]
    Qfi2 = [(1 + alpha / 100) * qf for qf in Qf2]
    Qt = [qm + qf1 + qf2 for qm, qf1, qf2 in zip(Qm, Qf1, Qf2)]
    Qmi = [qt-qf1-qf2 for qt,qf1,qf2 in zip(Qt,Qfi1,Qfi2) ]

    result = [(qf1, qm, qf2) for qf1, qm, qf2 in zip(Qfi1, Qmi, Qfi2)]
    return result


def round_time(str_time):
    """
    transform time format : 00:00:00,3 to 00:00:00 for example
    :param str_time: time in str
    :return:
    """
    split0 = str_time.split(':')
    L1, L2 = [], []

    for elt in split0:
        try:
            L1.append(str(int(elt)))
        except:
            L1.append(str(int(elt.split(',')[0])))

    for elt in L1:
        if len(elt) == 1:
            elt = '0' + elt
        L2.append(elt)
    reslt = ':'.join(L2)
    #     split = [elt.replace(',','.') for elt in split0]
    #     reslt = ':'.join([''.join(str(round(float(elt),0)).split('.'))[:2] for elt in split])
    return reslt

def convert_str_hours(str_):
    """
    convert time format : 00:00:00 to hours
    :param str_:
    :return:
    """
    if str_.split(':')[0] == '00':
        if str_.split(':')[-1]=='60':
            str_=str_.replace('60','59')
        time_ = datetime.strptime(str_[3:], '%H:%M:%S')
        time_hour = round(time_.hour + time_.minute / 60 + time_.second / 3600, 4)
    else:
        if str_.split(':')[-1]=='60':
            str_=str_.replace('60','59')
        time_ = datetime.strptime(str_, '%d:%H:%M:%S')
        time_hour = round(time_.day * 24 + time_.hour + time_.minute / 60 + time_.second / 3600, 4)

    return time_hour


# convert str of TRA File (CPU time) to datetime in hours
convert_str_hours2 = lambda x: round( datetime.strptime(x, '%H:%M:%S,%f').hour +
                                    datetime.strptime(x, '%H:%M:%S,%f').minute / 60 +
                                    datetime.strptime(x, '%H:%M:%S,%f').second / 3600 +
                                    datetime.strptime(x, '%H:%M:%S,%f').microsecond/1000000/3600, 4)

# TO DELETE
# def convert_str_min(str_):
#     """
#     convert time format : 00:00:00 to minutes
#     :param str_:
#     :return:
#     """
#     if str_.split(':')[-1]=='60':
#         str_=str_.replace('60','59')
#     time_ = datetime.strptime(str_, '%H:%M:%S')
#     time_minute = round(time_.hour*60 + time_.minute + time_.second /60, 4)
#
#     return time_minute


def find_Z(data,elt):
    around_data = data.iloc[(data['Pm(m)']-elt).abs().argsort()[:2]]
    if len(around_data)>1:
        Z_fond = list(around_data['Z_fond'])
        Pm = list(around_data['Pm(m)'])
        f= interp1d(Pm,Z_fond)
        return f(elt)
    else:
        return float(around_data['Z_fond'])

def find_Zi(sec, elt):
    secs_pk = pd.Series(list(sec.keys()))
    around_data = secs_pk.iloc[(secs_pk - elt).abs().argsort()[:2]]
    if len(around_data) > 1:
        Pm = list(around_data)
        sec_av, sec_ap = sec[Pm[0]].data['Z'], sec[Pm[1]].data['Z']
        zi = [interp1d(Pm, [av, ap]) for av, ap in zip(sec_av, sec_ap)]
        return [float(f(elt)) for f in zi]
    else:
        return sec[float(around_data)].data['Z']

def find_Xi(sec, elt):
    secs_pk = pd.Series(list(sec.keys()))
    around_data = secs_pk.iloc[(secs_pk - elt).abs().argsort()[:2]]
    if len(around_data) > 1:
        Pm = list(around_data)
        sec_av, sec_ap = sec[Pm[0]].data['abs'], sec[Pm[1]].data['abs']
        xi = [interp1d(Pm, [av, ap]) for av, ap in zip(sec_av, sec_ap)]
        return [float(f(elt)) for f in xi]
    else:
        return sec[float(around_data)].data['abs']

def find_Bi(mgefin, elt):
    around_data = mgefin.iloc[(mgefin['Pm(m)'] - elt).abs().argsort()[:2]]
    if len(around_data) > 1:
        Pm = list(around_data['Pm(m)'])
        Bi = list(around_data['Largeur'])
        f = interp1d(Pm, Bi)
        return float(f(elt))
        #         return around_data
    else:
        return float(around_data['Largeur'])
    return around_data

def add_Hmoy(LdE,Etd):
    """
    compute Hmoy for measures of LdE
    :param LdE: pandas with PK & Z as columns
    :param Etd: Etude compared with measures
    :return:
    """
    meshs=Etd.biefs.elements[0].mesh.elements
    mgf = Etd.Magefin.data
    zi_fond =LdE['Pk'].apply(lambda x: find_Zi(meshs,x))
    xi =LdE['Pk'].apply(lambda x: find_Xi(meshs,x))
    zi_eau = list(LdE['Z [mNGF]'])
    Smoy=[]
    for zif,zie,x in zip(zi_fond,zi_eau,xi):
        Smoy.append(trapz([zie]*len(x),x)-trapz(zif,x))
    LdE['Smoy'] = Smoy
    LdE['Largeur'] = LdE['Pk'].apply(lambda x: find_Bi(mgf,x))
    LdE['Hmoy'] = LdE['Smoy'] / LdE['Largeur']


def compute_Hmoy(sec,cote):
    data = sec.data
    ind_gauche, ind_droite = sec.get_intersections(cote)
    xi_between = list(data['abs'].iloc[ind_gauche:ind_droite])
    zi_between = list(data['Z'].iloc[ind_gauche:ind_droite])
    Z_eau = [cote] * len(xi_between)

    Smoy = trapz(Z_eau, xi_between) - trapz(zi_between, xi_between)
    B = xi_between[-1] - xi_between[0]
    return (Smoy, B, Smoy / B)

