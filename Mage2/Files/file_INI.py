from Mage2.Files.file_M import file_M

class file_INI:
    def __init__(self,name,path,args):
        self.model_name= name
        self.file_name = name + '.INI'
        self.path = path
        self.bief_nb = 1
        self.fond_indicator = 'fon'
        self.depth=1.
        self.discharge = 1.
        self.args = args

    def export_INI(self,option='HC',opt='w'):
        mesh = file_M(self.model_name,self.path, self.args)

        mesh.read_file()

        path = self.path+'/'+self.file_name
        with open(path,opt) as f:
            writing = mesh.write_file(self.bief_nb,self.discharge, self.fond_indicator, self.depth,option)
            f.write(writing)

        # print('INI File exported')

    def read_file(self):
        print('Reading file for INI files is not yet written')