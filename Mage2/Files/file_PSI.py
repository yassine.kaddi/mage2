class file_PSI:
    def __init__(self,name,path):
        self.name = name
        self.path = path
        self.file_name = name + '.PSI'

    def write_file(self, biefs):
        if len(biefs.elements) == 0:
            print('Biefs are empty!')
            return

        lines = []
        header = '*Bief        x deb     x fin     Psi_t\n*---======----------==========----------\n'

        lines.append(header)

        for elt in biefs.elements:
            for pki, psi in biefs.elements[elt].psi:
                line = 'P  ' + str(biefs.elements[elt].id) + '       ' + self.write_number(pki[0]) + '    ' + self.write_number(
                    pki[1]) + '       ' + self.write_number(psi)+ '\n'
                lines.append(line)

        lines = ''.join(lines)
        return lines

    def write_number(self, number):
        # to write a number in PSI format
        max_len = 5
        number_str = str(number)
        while len(number_str) < max_len:
            if '.' in number_str:
                integ, decm = number_str.split('.')
                decm = '.' + decm + '0'
                number_str = integ + decm
            else:
                number_str += '.00'

        number_str = number_str[:max_len]
        return number_str

    def read_file(self):
        print('Not yet written')