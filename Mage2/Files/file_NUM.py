from datetime import datetime
import re
from Mage2.utils import convert_str_hours


# works when Instant final is defined (not in 999:9999... format)
# supposes that instant initial is 0:00:00:00
# Only few NUM parameters are read and exported :


class file_NUM:
    def __init__(self,name,path):
        self.name= name
        self.path = path
        self.file_name = name + '.NUM'
        self.params = {} # keys: param_implic & final_time_hour & tmin, tmax & is_permanent & coeff_liss
                             # & reduc_pdt & reduc_ZQ

    def read_file(self):

        with open(self.path + '/'+self.file_name, 'r') as f:
            data = f.readlines()

        # Parametre d'implicitation:
        for elt in data[0].split(':')[1].split(' '):
            try:
                self.params['param_implic'] = float(elt)
            except:
                pass

        # Instant final in hours
        inst_fin_str = data[2].split(' ')[-1][:-1]
        self.params['final_time_hour'] = convert_str_hours(inst_fin_str)

        # if inst_fin_str.split(':')[0] == '00':
        #     time_ = datetime.strptime(inst_fin_str[3:], '%H:%M:%S')
        #     self.num_params['final_time_hour'] = round(time_.hour + time_.minute / 60 + time_.second / 3600, 4)
        # else:
        #     time_ = datetime.strptime(inst_fin_str, '%d:%H:%M:%S')
        #     self.num_params['final_time_hour'] = round(time_.day * 24 + time_.hour + time_.minute / 60 + time_.second / 3600, 4)

        # Permanent ou pas
        self.params['is_permanent'] = 'Permanent' in data[0]

        #  Pas de temps
        _,self.params['tmax'], self.params['tmin'] = [float(elt) for elt in re.findall('\d+[.]', data[3])]

        # Coefficient de lissage
        self.params['coeff_liss'] = float(re.findall('\d+[.]\d+', data[7])[0])

        # Fact de réduction pdt
        self.params['reduc_pdt'] = int(re.findall('\d+', data[11])[0])

        # Fact de réduction Z Q
        reducs = re.findall('\d+',data[13])
        self.params['reduc_ZQ'] = (int(reducs[0]), int(reducs[2]))



    def export_NUM(self,is_permanent):

        with open(self.path + '/'+self.file_name, 'r') as f:
            data = f.readlines()
        data2 = data

        # export param implic
        data2[0] = self.write_param_implic(self.params['param_implic'],is_permanent)

        # Export pas de temps
        data2[3] = self.write_pdt(self.params['tmax'], self.params['tmin'])

        # Export coeff de lissage
        data2[7] = self.write_coeff_liss(self.params['coeff_liss'])

        # Export Fact de réduction pdt
        data2[11] = self.write_reduct_pdt(self.params['reduc_pdt'])

        # Export Fact de réduction Z Q
        data2[13] = self.write_reduct_ZQ(self.params['reduc_ZQ'])


        with open(self.path + '/'+self.file_name,'w') as file:
            file.writelines(data2)


    def write_param_implic(self,par,is_permanent=False):
        if is_permanent:
            return " Parametre d'implicitation.......[0.7].: {} S B R Permanent\n".format(par)
        else:
            return " Parametre d'implicitation.......[0.7].: {} S B R \n".format(par)
    def write_pdt(self,t_max, t_min):
        return " Pas de temps en SECONDES.......[300.].:      {}     {}\n".format(float(t_max), float(t_min))

    def write_coeff_liss(self,par):
        return ' Coefficient de lissage...........[0.].:    {:.5f}\n'.format(par)

    def write_reduct_pdt(self,par):
        return ' Facteur de reduction du pas de temps..: {}\n'.format(int(par))

    def write_reduct_ZQ(self,pars):
        return ' Facteur de reduction de precision Z Q.: {}+0 {}+0\n'.format(int(pars[0]), int(pars[1]))