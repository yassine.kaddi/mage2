import re


class file_TRA:

    def __init__(self,name,path):
        self.name= name
        self.path = path
        self.file_name = name+'.TRA'
        self.params={}


    def read_file(self):
        sign_Duree = 'CPU'

        try:
            with open(self.path+'/'+self.file_name, 'r') as f:
                data = f.readlines()
            lines = [elt for elt in data if sign_Duree in elt]
            self.params['time'] = re.findall('\d+[:]\d+[:]\d+[,]\d+', lines[0])[0]

        except:
            self.params['time'] = '00:00:00'
