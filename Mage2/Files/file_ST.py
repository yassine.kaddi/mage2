import pandas as pd

class file_ST:

    def __init__(self,args=['RD','RG','fon']):
        self.sections= []
        self.name = ''
        self.RD_label = args[0]
        self.RG_label = args[1]
        self.fond_label = args[2]

    def write_file(self ,sections_obj,max_len):
        sections = sections_obj.data
        # write a section bloc in ST file
        if (len(sections.X) != len(sections.Y)) or (len(sections.X) != len(sections.Y)):
            print('X, Y and Z have different lengths!')
            return

        nb_pts =str(len(sections.X))
        header= '     ' +str(sections_obj.indice) +'     ' +'0' +'     ' +'0' +'   ' +nb_pts\
                +'   ' +self.write_number(sections_obj.pk,max_len) +'   '+ sections_obj.Nom_section +'\n'
        lines= []
        lines.append(header)
        for elt in range(len(sections.X)):
            line =  '  ' +self.write_number(sections.X.iloc[elt],max_len) +' ' +self.write_number \
                (sections.Y.iloc[elt],max_len) +'     ' +self.write_number(sections.Z.iloc[elt],7) +' '+ sections.com.iloc[elt] +'\n'
            lines.append(line)
        last_line = '     999.9990     999.9990     999.9990 \n'
        lines.append(last_line)
        lines =''.join(lines)
        return lines

    def write_number(self ,number,max_len):
        # to write a number in ST format (4 decim)
        # max_len =6
        number_str = str(number)
        while len(number_str) <max_len:
            if '.' in number_str:
                integ ,decm = number_str.split('.')
                decm ='.' + decm + '0'
                number_str = integ + decm
            else:
                number_str += '.0000'

        number_str = number_str[:max_len]
        return number_str

    def get_data_meshing(self,path):
        data = pd.read_csv(path, header=None, encoding = "ISO-8859-1", sep="\s+",skiprows=3, usecols=[0,1,2,3,4])
        data.columns = ['X','Y','Z','Rives','PK']

        data['PK'] = data['PK'].fillna(method='ffill')

        RD = data[['PK','Y','Z']].loc[data['Rives']==self.RD_label]
        RG = data[['PK','Y','Z']].loc[data['Rives']==self.RG_label]
        Fond = data[['PK','Y','Z']].loc[data['Rives']==self.fond_label]

        return (RD,RG,Fond)

    def read_file(self):
        print('Reading file for ST files is not yet written, but reading M file can be used')