import re

# only read time simulation for the moment

class file_Output:
    def __init__(self,name,path):
        self.name= name
        self.path = path
        self.file_name = 'output'
        self.params={}

    def read_file(self):
        sign_T = " \\\\\\ T:"
        sign_DT = " DT ="
        sign_dZ = ' dZ/dt ='

        try:
            with open(self.path + '/'+self.file_name, 'r') as f:
                data = f.readlines()
            lines = [elt for elt in data if sign_T in elt]
            self.params['time_sim'] = re.findall('\d+[:]\d+[:]\d+[:]\d+', lines[-1])[0]
            self.params['Courant'] = [float(re.findall('\d+[.]\d+', elt)[0]) for elt in lines]
            self.params['residu'] = [float(re.findall('\d+[.]\d+', elt)[2]) for elt in lines]
            self.params['Froude'] = [float(re.findall('\d+[.]\d+', elt)[-2]) for elt in lines]
        except:
            self.params['time_sim']='00:00:00:00'
            self.params['Courant'] = [0]
            self.params['residu'] = [0]
            self.params['Froude'] = [0]

        try:
            with open(self.path + '/'+self.file_name, 'r') as f:
                data = f.readlines()
            lines = [elt for elt in data if sign_DT in elt and sign_dZ in elt]
            self.params['DT'] = [float(re.findall('\d+[.]\d+', elt)[0]) for elt in lines]
        except:
            self.params['DT'] = [0]



            # return data

    def simulation_stopped(self):
        try:
            file = open(self.path+'/'+self.file_name)
            for i in range(200):
               if 'mauvaise discrétisation' in file.readline():
                   return True
        except:
            return True
        return False

