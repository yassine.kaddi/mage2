class file_NET:
    def __init__(self,name,path):
        self.model_name= name
        self.path = path
        self.file_name = self.model_name+'.net'
        self.total_nb = 1
        self.bief_data = {}

    def read_file(self):
        with open(self.path+'/'+self.file_name,'r') as file:
            data = file.readlines()
        self.total_nb = len(data)

        for elt in range(len(data)):
            self.bief_data[elt] = self.get_data(data[elt])


    def get_data(self,line):
        bief_name,node_upstream,node_downstream = line.split(' ')[:3]
        return bief_name,node_upstream,node_downstream