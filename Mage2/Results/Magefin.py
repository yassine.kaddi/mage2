import pandas as pd
import numpy as np
import os

g = 9.81
class Magefin:

    '''QQ infos à changer en cas de généralisation:
        >les *1000 pr laisser débit en m3/s et hauteurs en mm
        >le .117 différence entre mc et fp
        > Rayon hydraulique calculé pr mon cas
        > fichiers .csv (variation en fct du temps) calculées pr mon cas
        > je prends que Qf gauche et Qmineur'''

    def __init__(self,name,path,option):
        self.data = pd.DataFrame([])
        self.path = path
        self.folders = {}
        self.model_name = name
        self.option = option

    def modify_magefin(self):
        """
        modify mage_fin.ini to stick "#" element to the number
        :return:
        """
        with open(self.path + '/Mage_fin.ini', 'r') as f:
            lines = f.readlines()
        new_lines = []
        for elt in lines:
            if '#' in elt:
                new_lines.append('#'.join(elt.split(' #')))
            else:
                new_lines.append(elt)
        new_lines = ''.join(new_lines)

        with open(self.path + '/Mage_fin_python.ini', 'w') as f:
            f.write(new_lines)


    def read_file(self):
        """
        To read the mage_fin.fin file
        :param option: 'D' Debord or 'I' ISM. BY default it takes ISM
        :return: pandas of magefin file
        """
        try:
            # Debord
            if self.option == 'D':
                L = np.arange(24)
                cols = ['IB', 'IS', 'Q', 'Cote(m)', 'Pm(m)', 'Z_fond', 'Z_moyen', 'Z_berge', 'H', 'Largeur', 'Fr', 'V', 'Q_Lat',
                        'Qdev', 'K_min',
                        'K_maj', 'Surface', 'Périmètre', 'Q_mineur', 'Q_moyen', 'V_mineur', 'V_moyen', 'Débitance',
                        'Boussinesq']

                data = pd.read_csv(self.path + '/Mage_fin.ini', encoding="ISO-8859-1", sep="\s+", skiprows=9, usecols=L, names=cols)
                data['H'] = data['H'].apply(lambda row: float(''.join([str(s) for s in row if s.isdigit()])))
                mineur = data['Q_mineur']/data['V_mineur']
                moyen = data['Q_moyen']/data['V_moyen']
                data['Smoy'] = mineur + moyen
                data['Hmoy'] = data['Smoy']/data['Largeur']
                print('Magefin is read in ', self.path)
                self.data = data
            # ISM
            elif self.option == 'I':
                self.modify_magefin()
                L = np.arange(30)
                cols = ['IB', 'IS', 'Q', 'Cote(m)', 'Pm(m)', 'Z_fond', 'Z_moyen', 'Z_berge', 'H_FP_g%', 'H_FP_d%', 'Largeur',
                        'Largeur_D','Largeur_G','Fr', 'K_gauche', 'K_mineur', 'K_droite', 'Q_gauche', 'Q_mineur', 'Q_droite', 'V_gauche',
                        'V_mineur', 'V_droite', 'V_moyenne', 'Q_ex_G', 'Q_ex_D', 'tau_lm', 'tau_rm', 'V_int_left',
                        'V_int_right']

                raw_df = pd.read_csv(self.path + '/Mage_fin_python.ini', encoding="ISO-8859-1", sep="\s+", skiprows=9, usecols=L, names=cols)

                raw_df['H_FP_d%'] = raw_df['H_FP_d%'].apply(lambda row: float(''.join([str(s) for s in row if s.isdigit()])))
                raw_df['H_FP_g%'] = raw_df['H_FP_g%'].apply(lambda row: float(''.join([str(s) for s in row if s.isdigit()])))
                raw_df['Qr'] = (raw_df['Q_gauche'] + raw_df['Q_droite']) / raw_df['Q']
                gauche = (raw_df['Q_gauche'] / raw_df['V_gauche']).replace([np.inf, -np.inf], np.nan).fillna(0)
                droite = (raw_df['Q_droite'] / raw_df['V_droite']).replace([np.inf, -np.inf], np.nan).fillna(0)
                centre = raw_df['Q_mineur'] / raw_df['V_mineur']
                raw_df['Smoy'] = gauche + centre + droite
                raw_df['Hmoy'] = raw_df['Smoy']/raw_df['Largeur']


                print('Magefin is read in ', self.path)
                self.data = raw_df
            else:
                print('Mage fin is not read, check option (D or I)!')
        except:
            print('Problem in Magefin Reading. Check Magefin file in ', self.path)




    def read_many(self,path,option='I'):
        '''
         Read many simulation folders except those with "NC" in folder name
        :param path: of simulations
        :param option: 'I' for ISM and 'D' for Debord
        :return:
        '''

        folders = os.listdir(path)
        for folder in folders:
            elt = Magefin(self.model_name,path + '/' + folder,option)
            elt.read_file()
            self.folders[folder] = elt

    def get_Sfi(self):
        print('** Revoir le calcul de Rh_f et Rh_m')
        self.data['Sff'] = (1 / self.data['Kf']) ** 2 * self.data['Vf'] ** 2 / self.data['Rh_f'] ** (4 / 3)
        self.data['Sfm'] = (1 / self.data['Km']) ** 2 * self.data['Vm'] ** 2 / self.data['Rh_m'] ** (4 / 3)


    def compute_psi(self):
        Am, Af = self.data['Qm'] / self.data['Vm'], self.data['Qf'] / self.data['Vf']
        denom = 2 / Am + 1 / Af

        self.data['psi']= g * (self.data['Sff'] - self.data['Sfm']) / (self.data['Hf'] * (self.data['Vm'] - self.data['Vf']) ** 2) / denom


    def get_lambda(self):
        return np.mean((self.data['Vm'] - self.data['Vf']) / (self.data['Vm'] + self.data['Vf']))

    def correct_Vint(self):
        """
        corrige les valeurs Uint en aval du bief (=0 dans le calcul)
        :return:
        """

        path = self.path+'/'+self.model_name+'.TRA'
        with open(path) as f:
            data = f.readlines()

        indx=[i for i, elt in enumerate(data) if '-w' in elt][0]
        self.weight=float(data[indx].split('-w=')[1][:3])

        self.data['Vint_corrected_left'] = self.weight * self.data['V_mineur'] + (1 - self.weight) * self.data['V_gauche']
        self.data['Vint_corrected_right'] = self.weight * self.data['V_mineur'] + (1 - self.weight) * self.data[
            'V_droite']


    def compute_Pm13_q13_Pm23_q23(self):
        """
        recalcule q quand y a 3 biefs
        :return:
        """

        # coté gauche
        Qg_Bief1 = list(self.data['Q_gauche'].loc[self.data['IB'] == 1])
        Pmg_Bief1 = list(self.data['Pm(m)'].loc[self.data['IB'] == 1])

        Qg_Bief3 = list(self.data['Q_gauche'].loc[self.data['IB'] == 3])
        Pmg_Bief3 = list(self.data['Pm(m)'].loc[self.data['IB'] == 3])

        if Pmg_Bief1[-1]== Pmg_Bief3[0]:
            Qg_Bief13 = np.array(Qg_Bief1 + Qg_Bief3[1:])
            Pmg_Bief13 = np.array(Pmg_Bief1 + Pmg_Bief3[1:])
        else:
            Qg_Bief13 = np.array(Qg_Bief1 + Qg_Bief3)
            Pmg_Bief13 = np.array(Pmg_Bief1 + Pmg_Bief3)

        q13 = (Qg_Bief13[1:] - Qg_Bief13[:-1]) / (Pmg_Bief13[1:]-Pmg_Bief13[:-1])

        # coté droit
        Qd_Bief2 = list(self.data['Q_droite'].loc[self.data['IB'] == 2])
        Pmd_Bief2 = list(self.data['Pm(m)'].loc[self.data['IB'] == 2])

        Qd_Bief3 = list(self.data['Q_droite'].loc[self.data['IB'] == 3])
        Pmd_Bief3 = list(self.data['Pm(m)'].loc[self.data['IB'] == 3])

        if Pmd_Bief2[-1] == Pmd_Bief3[0]:
            Qd_Bief23 = np.array(Qd_Bief2 + Qd_Bief3[1:])
            Pmd_Bief23 = np.array(Pmd_Bief2 + Pmd_Bief3[1:])
        else:
            Qd_Bief23 = np.array(Qd_Bief2 + Qd_Bief3)
            Pmd_Bief23 = np.array(Pmd_Bief2 + Pmd_Bief3)

        q23 = (Qd_Bief23[1:] - Qd_Bief23[:-1]) / (Pmd_Bief23[1:] - Pmd_Bief23[:-1])

        return (Pmg_Bief13[:-1],q13,Pmd_Bief23[:-1],q23)



    def correct_q(self):
        pd.options.mode.chained_assignment = None
        """
        corrige les valeurs q en aval du bief (=0 dans le calcul)
        :return:
        """
        biefs_id=list(self.data['IB'].unique())

        for bif in biefs_id:
            data = self.data.loc[self.data['IB']==bif]
            last_is = data['IS'].iloc[-1]
            last_id = data.index[data['IS']==last_is][0]
            if float(data.loc[last_id, ['Q_ex_G']].iloc[0])==0:
                data.at[last_id, 'Q_ex_G'] = data.at[last_id-1, 'Q_ex_G']
            if  float(data.loc[last_id, ['Q_ex_D']].iloc[0])==0:
                data.at[last_id, 'Q_ex_D'] = data.at[last_id-1, 'Q_ex_D']
            self.data.loc[self.data['IB'] == bif] = data

    def relative_error(self,measures_df,col_pk,col_meas_par,col_sims_par):
        """
        compute relative error of magefin data and measures
        :param measures_df: measures in dataframe format
        :param col_pk: column name of pk in measures_df
        :param col_meas_par: column name of the parameter in measures_df
        :param col_sims_par: column name of the parameter in magefin
        :return: relative error
        """
        results = self.data
        if len(results)>0:
            sims_interp = [np.interp(elt, results['Pm(m)'], results[col_sims_par]) for elt in measures_df[col_pk]]
            RE = [(a - b)/b * 100 for (a, b) in zip(sims_interp, measures_df[col_meas_par])]
            return RE

        else:
            print('Magefin not read yet')
            return



