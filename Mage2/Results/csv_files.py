import pandas as pd
from datetime import datetime
import glob
import os
from Mage2.Results.Magefin import Magefin


class csv_files:
    def __init__(self,name,path,option):
        self.name = name
        self.path = path
        self.q_values = {}
        self.last_time_step = 0
        self.weight=0
        self.Z_DT = {}
        self.Q_DT = ()
        self.mage_fin = Magefin(name,path,option)

    def read_z_files(self):
        """
                to read the Z files exported in different Pk pts using *.REP file

                :param path: path of the exported files
                :return: dictionary of different Pk positions and Z(t) in those PK
                """
        par_xi = [(elt.split('/')[-1].split('.')[0].split('_')[1], int(elt.split('/')[-1].split('.')[0].split('_')[-1]))
                  for elt in glob.glob(self.path + '/*.csv')]

        # we gather the files of each parameter in a list. Only have z here:
        files_z = [elt for elt in par_xi if elt[0] == 'z']
        if len(files_z)==0:
            print('No Z csv files are found!')
        pathi = [elt for elt in glob.glob(self.path + '/*.csv') if elt.split('/')[-1].split('.')[0].split('_')[1] == 'z']
        nms = [['t(s)', 'Z(x=' + str(i[1]) + 'm)'] for i in files_z]

        # files reading
        Zi = {j[1]: pd.read_csv(elt, header=None, encoding="ISO-8859-1", sep="\s+", skiprows=1, names=i) for (j, elt, i)
              in zip(files_z, pathi, nms)}

        self.mage_fin.read_file()
        bed_depths = self.mage_fin.data[['Pm(m)', 'Z_fond']]

        # converting time to sec & depths to mm and adding the Hmoy: mean water depths
        for (elt, i) in zip(Zi.keys(), files_z):
            Zi[elt].iloc[:, 0] = Zi[elt].iloc[:, 0] * 3600
            Zi[elt].iloc[:, 1] = Zi[elt].iloc[:, 1] * 1000
            #     hauteurs d'eau
            Zi[elt]['Hm(x=' + str(i[1]) + 'm)'] = Zi[elt].iloc[:, 1] - float(
                bed_depths['Z_fond'].loc[bed_depths['Pm(m)'] == i[1]]) * 1000

            self.Z_DT = Zi
            # Zi[elt]['Hf(x=' + str(i[1]) + 'm)'] = Zi[elt]['Hm(x=' + str(i[1]) + 'm)'] - bankful * 1000
            # S_mouille = Zi[elt]['Hm(x=' + str(i[1]) + 'm)'] * Bm + Zi[elt]['Hf(x=' + str(i[1]) + 'm)'] * Bf
            # Zi[elt]['Hmoy(x=' + str(i[1]) + 'm)'] = S_mouille / B

            # Rh = {}
            # Rh['MC'] = Zi[elt]['Hm(x=' + str(i[1]) + 'm)'] * Bm / (
            #             Zi[elt]['Hm(x=' + str(i[1]) + 'm)'] + Bm + bankful * 1000)
            # Rh['FP'] = Zi[elt]['Hf(x=' + str(i[1]) + 'm)'] * Bf / (Zi[elt]['Hf(x=' + str(i[1]) + 'm)'] + Bf)

    def read_qi_files(self):
        """
        To read the exported hydrographs Qtotal(t), Qmainchannel(t), Qfloodplain(t) using *.REP File
        :param path: path where the files are exported
        :return:tuple of pandas of Q(L/s) as function of time (s)
        """

        par_xi = [(elt.split('/')[-1].split('.')[0].split('_')[1], int(elt.split('/')[-1].split('.')[0].split('_')[-1]))
                  for elt in glob.glob(self.path + '/*.csv')]
        #       we gather the files of each parameter in a list. have qm & qr here:
        path_qm, path_qr = [elt for elt in glob.glob(self.path + '/*.csv') if
                            elt.split('/')[-1].split('.')[0].split('_')[1] == 'qm'], [elt for elt in
                                                                                      glob.glob(self.path + '/*.csv') if
                                                                                      elt.split('/')[-1].split('.')[
                                                                                          0].split('_')[1] == 'ql']

        print('Qr files reading is not implemented yet !!')
        files_qm, files_qr = [elt for elt in par_xi if elt[0] == 'qm'], [elt for elt in par_xi if elt[0] == 'ql']
        nms_qm, nms_qr = [['t(s)', 'Qm(x=' + str(i[1]) + 'm)'] for i in files_qm], [['t(s)', 'Ql(x=' + str(i[1]) + 'm)']
                                                                                    for i in files_qr]

        if len(files_qm)==0:
            print('No Qm CSV files are found')
        else:
            print('Qm CSV files are read')

        if len(files_qr)==0:
            print('No Ql CSV files are found')
        else:
            print('Ql CSV files are read')

        #         files reading
        Qmi = {j[1]: pd.read_csv(elt, header=None, encoding="ISO-8859-1", sep="\s+", skiprows=1, names=i) for
               (j, elt, i) in zip(files_qm, path_qm, nms_qm)}
        Qri = {j[1]: pd.read_csv(elt, header=None, encoding="ISO-8859-1", sep="\s+", skiprows=1, names=i) for
               (j, elt, i) in zip(files_qr, path_qr, nms_qr)}
        for (qm, m) in zip(Qmi.keys(), files_qm):
            Qmi[qm].iloc[:, 0] = Qmi[qm].iloc[:, 0] * 3600
            Qmi[qm].iloc[:, 1] = Qmi[qm].iloc[:, 1] * 1000
            #           vels (cm/s), considering that the width Bm=Br=1
            Qmi[qm]['U(x=' + str(m[1]) + 'm)'] = Qmi[qm].iloc[:, 1] / self.Z_DT[qm].iloc[:, 2]
        for (qr, r) in zip(Qri.keys(), files_qr):
            Qri[qr].iloc[:, 0] = Qri[qr].iloc[:, 0] * 3600
            Qri[qr].iloc[:, 1] = Qri[qr].iloc[:, 1] * 1000
            #           vels (cm/s), considering that the width Bm=Br=1
        #     Qri[qr]['U(x=' + str(r[1]) + 'm)'] = Qri[qr].iloc[:, 1] / self.Z_DT[qr].iloc[:, 3]

        self.Q_DT = (Qmi, Qri)



    def get_last_time(self):

        P = self.path+'/Mage_fin.ini'

        with open(P,'r') as file:
            file.readlines()
        with open(P,'r') as file:
            data = [x.strip() for x in file]

        dte = data[1][49:60]
        if dte.split(':')[0]=='00':
            time_ = datetime.strptime(data[1][52:60], '%H:%M:%S')
            self.last_time_step = round(time_.hour + time_.minute / 60 + time_.second / 3600, 4)
        else:
            time_ = datetime.strptime(data[1][49:60], '%d:%H:%M:%S')
            self.last_time_step = round(time_.day * 24 + time_.hour + time_.minute / 60 + time_.second / 3600, 4)

    def get_parameter_RESfile(self,extraire_name,files_names,parameter, nb_bief):
        """
        get parameter from RES Files (many files when we have many branches)
        :param extraire_name: mage_extraire or the one defined (character)
        :param files_names: output names (list of characters)
        :param parameter: (character)
        :param nb_bief: (list of characters)
        :param path:
        :return:
        """

        self.get_last_time()
        os.chdir(self.path)
        for nb,file in zip(nb_bief,files_names):
            command = extraire_name+' '+self.name + ' ' + parameter + ' ' + nb+ ' h'+ str(self.last_time_step)
            os.system(command)
            os.system('mv '+self.name+'.res '+file+'.res')
            print('Commande pour RES : ', command )

        data = [pd.read_csv(self.path+'/'+file+'.res', header=None,encoding="UTF-8", sep="\s+", skiprows=3,names=['Pk',parameter]) for file in files_names]
        self.q_values[parameter] = data
