import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy.interpolate import interp1d

mpl.rcParams['figure.figsize'] = (13.0, 8)
mpl.rc('xtick',labelsize=15)
mpl.rc('ytick',labelsize=15)
mpl.rc('legend', fontsize=15)

class sections:

    def __init__(self,name,path, args):
        self.model_name = name
        self.path = path
        self.args = args
        self.file_name = 'Bief_1.ST'
        self.content = ''
        self.elements = {}

    def read_sections(self,ref=2):

        path = self.path + '/MailleurPF/' + self.file_name
        with open(path, 'r') as f:
            self.content = f.read()

        decomposed = self.decompose_file_txt()
        Pms = [float(self.get_section_Pm(profi)[1]) for profi in decomposed if len(profi) > 0]
        names= [self.get_section_Pm(profi)[2] for profi in decomposed if len(profi) > 0]
        profiles = [pro.split('\n') for pro in decomposed]
        i=0
        for profile, Pm,name in zip(profiles, Pms,names):
            sec = section(self.model_name, self.path,self.args)
            XYZ_com = [self.get_XYZ(line) for line in profile[1:]]

            sec.pk = round(Pm,2)
            sec.indice = i+1
            sec.data = pd.DataFrame({'X': [elt[0] for elt in XYZ_com[:-1]],
                                   'Y': [elt[1] for elt in XYZ_com[:-1]],
                                   'Z': [elt[2] for elt in XYZ_com[:-1]],
                                   'com': [elt[3] for elt in XYZ_com[:-1]]})
            sec.Nom_section = name
            if (sec.data['X'].iloc[0]!=sec.data['X'].iloc[-1]):
                sec.compute_abscisse(ref)
            else:
                sec.data['abs'] = sec.data['Y']

            self.elements[round(Pm,2)] = sec
            i+=1


    def decompose_file_txt(self):
        """
        compose meshing file **.M to a list of profiles
        :return:
        """
        separatrice = [elt for elt in self.content.split('\n') if '999.99' in elt][0]+'\n'
        data = self.content.split(separatrice)
        return data

    def get_XYZ(self,line):
        no_empty = [elt for elt in line.split(' ') if len(elt) > 0]
        X, Y, Z, com = 1, 1, 1, ''
        if len(no_empty) == 3:
            X, Y, Z = no_empty
            com = ''
        elif len(no_empty) == 4:
            X, Y, Z, com = no_empty
        else:
            pass

        return float(X), float(Y), float(Z), com

    def get_section_Pm(self,profile):
        """
        get profile index & Pm
        :param profile: result of compose_file
        :return:
        """
        line = profile.split('\n')[0]
        split = line.split(' ')
        no_empty = [elt for elt in split if len(elt) > 0]
        is1 = no_empty[0]

        try:
            float(no_empty[-1])
            Pm = no_empty[-1]
            name = ''
        except:
            Pm = no_empty[-2]
            name = no_empty[-1]

        return is1,Pm, name



class section:
    """
    - pk : Pk of the cross section
    - Nom_section : name of cross-section
    - indice : index of the section
    - data : coordinates of section
    - plotme : plot the cross section
    - proprieties (...)
    """

    def __init__(self,name,path,args=('rg','rd', 'fon')):
        TYPES = ['ProfilXYZ', 'ProfilAC', 'ProfilLC', 'ProfilR', 'ProfilT', 'ProfilRT']
        #         self.nb_pts = 0
        self.pk = 0
        self.Nom = []
        self.model_name = name
        self.path = path
        self.Nom_section = ''
        self.slope = 0
        self.indice=1
        self.RD_label = args[1]
        self.RG_label = args[0]
        self.fond_label = args[2]

        self.data = pd.DataFrame()
        self.data_equiv = pd.DataFrame()  # RD RG modified to be in the same sides for all bief cross sections for plot matters
    #         self.data = {'x':self.x,'y':self.y,'z':self.z,'nom':[],'Commentaire':[],'abs':[]}

    def compute_abscisse(self, ref=2):
        """
        compute abscisse for selfion self defined by XYZ. the origine is in fon (ref=1), RD (ref=2) or RG (ref=3)
        :param data: selfion data
        :return:
        """
        if ref == 1:
            labl = self.fond_label
        elif ref == 2:
            labl = self.RD_label
        elif ref == 3:
            labl = self.RG_label
        else:
            print('ref = 1(fond) 2(rd) or 3(rg)')


        refX = self.data['X'].loc[self.data['com'] == labl]
        refY = self.data['Y'].loc[self.data['com'] == labl]
        DX = self.data['X'] - float(refX)
        DY = self.data['Y'] - float(refY)

        ind_rd = self.data[self.data['com']==self.RD_label.lower()].index.values[0]

        # data_rd_side = sec.loc[sec['Y'] <= y_rd]
        # data_rd_side = self.data.loc[:self.data['com'].str.contains(self.RD_label).idxmax()]

        signs = []
        for x, y, i in zip(DX, DY, range(len(DX))):
            if i < ind_rd:
                signs.append(-1)
            else:
                signs.append(1)

        self.data['abs'] = signs * np.sqrt(DX ** 2 + DY ** 2)


    def plotme(self,cote=0,fontsize=17,path_save=''):

        # plot raw sections
        if len(self.data)>0:
            abs_,Z_ax = self.data['abs'], self.data['Z']
            plt.plot(abs_,Z_ax,'o-')
            if len(abs_.loc[self.data['com'].str.lower()==self.RD_label.lower()])>0:
                plt.text(abs_.loc[self.data['com'].str.lower()==self.RD_label.lower()],Z_ax.loc[self.data['com'].str.lower()==self.RD_label.lower()],self.RD_label,fontsize=fontsize)
            else:
                print('No RD label')

            if len(abs_.loc[self.data['com'].str.lower()==self.RG_label.lower()])>0:
                plt.text(abs_.loc[self.data['com'].str.lower()==self.RG_label.lower()],Z_ax.loc[self.data['com'].str.lower()==self.RG_label.lower()],self.RG_label,fontsize=fontsize)
            else:
                print('No RG label')

            if len(abs_.loc[self.data['com'].str.lower()==self.fond_label.lower()])>0:
                plt.text(abs_.loc[self.data['com'].str.lower()==self.fond_label.lower()],Z_ax.loc[self.data['com'].str.lower()==self.fond_label.lower()],self.fond_label,fontsize=fontsize)
            else:
                print('No fond label')

            if cote!=0:
                plt.plot(abs_, [cote]*len(abs_))
            plt.xlabel('abs (m)',fontsize=fontsize)
            plt.ylabel('Z(m)',fontsize=fontsize)

            if len(path_save) > 0:
                plt.savefig(path_save)

            plt.show()
        else:
            print('No data')

    # def plotme2(self,cote=0,fontsize=17):
    #
    #     # plot modified sections (data_equiv)
    #     if len(self.data_equiv)>0:
    #         Y_ax,Z_ax = self.data_equiv['Y'], self.data_equiv['Z']
    #         plt.plot(Y_ax,Z_ax,'o-')
    #         plt.text(Y_ax.loc[self.data_equiv['com'].str.lower()==self.RD_label],Z_ax.loc[self.data_equiv['com'].str.lower()==self.RD_label],self.RD_label,fontsize=fontsize)
    #         plt.text(Y_ax.loc[self.data_equiv['com'].str.lower()==self.RG_label],Z_ax.loc[self.data_equiv['com'].str.lower()==self.RG_label],self.RG_label,fontsize=fontsize)
    #         plt.text(Y_ax.loc[self.data_equiv['com'].str.lower()==self.fond_label],Z_ax.loc[self.data_equiv['com'].str.lower()==self.fond_label],self.fond_label,fontsize=fontsize)
    #         if cote!=0:
    #             plt.plot(Y_ax, [cote]*len(Y_ax))
    #         plt.xlabel('Y(m)',fontsize=fontsize)
    #         plt.ylabel('Z(m)',fontsize=fontsize)
    #
    #         plt.show()
    #     else:
    #         print('No data')

    def compute_section(self, section2, pos='upstream'):
        """
        compute new cross section coordinate when it doesn't change from section2, situated upstream or downstream section2
        :param section2: cross-section copied
        :param pos: position: 'upstream' or 'downstream'
        """

        self.indice = section2.indice + 1
        # self.slope = section2.slope
        # self.type = section2.type
        self.data['Y'] = section2.data['Y']
        self.data['com'] = section2.data['com']
        if self.slope ==0:
            print('Need to define slope')
        else:
            try:
                if pos == 'upstream':
                    self.data['X'] = section2.data['X'] - section2.pk
                    self.data['Z'] = section2.data['Z'] + self.slope * np.abs(section2.pk - self.pk)

                if pos == 'downstream':
                    self.data['X'] = section2.data['X'] + section2.pk
                    self.data['Z'] = section2.data['Z'] - self.slope * np.abs(section2.pk - self.pk)
            except:
                print('Problem in Z computation')

    def check_rives(self):

        self.data_equiv = self.data.copy()

        try:
            y_rd = float(self.data['Y'].loc[self.data['com'] == self.RD_label])

        except:
            y_rd = float(self.data['Y'].iloc[0])
            print('No RD detected !')

        try:
            y_rg = float(self.data['Y'].loc[self.data['com'] == self.RG_label])
        except:
            y_rg = float(self.data['Y'].iloc[-1])
            print('No RG detected !')

        cdt = y_rd < y_rg

        if cdt:
            rd = self.RD_label
            rg = self.RG_label
        else:
            rd = self.RG_label
            rg = self.RD_label

        self.data_equiv.loc[self.data['com'].str.lower() == self.RD_label, 'com'] = rd
        self.data_equiv.loc[self.data['com'].str.lower() == self.RG_label, 'com'] = rg

        return (rd,rg,cdt)

    def get_limites(self):
        """
        get X & Y limits of FP
        :param option: option=1 in case RG and RD were inversed in pamhyr. Otherwise: option=0
        :return:
        """
        rd, rg,_ = self.check_rives()

        if len(self.data_equiv)>0:

            Y_lim_sup_FP_gauche, X_lim_sup_FP_gauche = self.data_equiv['Y'].iloc[0], self.data_equiv['X'].iloc[0]
            Y_lim_sup_FP_droite, X_lim_sup_FP_droite = self.data_equiv['Y'].iloc[-1], self.data_equiv['X'].iloc[-1]
            try:
                Y_lim_inf_FP_gauche, X_lim_inf_FP_gauche = float(self.data_equiv['Y'].loc[self.data_equiv['com']==rg]), float(self.data_equiv['X'].loc[self.data_equiv['com']==rg])
                Y_lim_inf_FP_droite, X_lim_inf_FP_droite = float(self.data_equiv['Y'].loc[self.data_equiv['com']==rd]), float(self.data_equiv['X'].loc[self.data_equiv['com']==rd])


            except:
                print('no RG or RD in pk ', self.pk)
                Y_lim_inf_FP_gauche, X_lim_inf_FP_gauche = 0,0
                Y_lim_inf_FP_droite, X_lim_inf_FP_droite = 0,0
            try:
                Y_fon, X_fon = float(self.data_equiv['Y'].loc[self.data_equiv['com'].str.lower()==self.fond_label.lower()]), float(self.data_equiv['X'].loc[self.data_equiv['com'].str.lower()==self.fond_label.lower()])
            except:
                # print('no bottom label')
                Y_fon, X_fon = 0,0
            return (X_lim_sup_FP_gauche, Y_lim_sup_FP_gauche,
                    X_lim_inf_FP_droite, Y_lim_inf_FP_droite,
                    X_lim_inf_FP_gauche, Y_lim_inf_FP_gauche,
                    X_lim_sup_FP_droite, Y_lim_sup_FP_droite,
                    X_fon, Y_fon)
        else:
            print('No data')
            return

    def get_Z_limites(self,cote):
        """
        get X (abscisse) & Y (altitude) limits of FP at given altitude cote
        :param option: option=1 in case RG and RD were inversed in pamhyr. Otherwise: option=0
        :return:
        """
        rd, rg,cdt = self.check_rives()

        if len(self.data_equiv)>0:

            coords_D,coords_G = self.interpolate_Z(cote)
            X_lim_sup_FP_gauche, Y_lim_sup_FP_gauche = coords_G
            X_lim_sup_FP_droite, Y_lim_sup_FP_droite = coords_D
            try:
                Y_lim_inf_FP_gauche, X_lim_inf_FP_gauche = float(self.data_equiv['Y'].loc[self.data_equiv['com']==rg]), float(self.data_equiv['X'].loc[self.data_equiv['com']==rg])
                Y_lim_inf_FP_droite, X_lim_inf_FP_droite = float(self.data_equiv['Y'].loc[self.data_equiv['com']==rd]), float(self.data_equiv['X'].loc[self.data_equiv['com']==rd])
            except:
                print('no RG or RD in pk ', self.pk)
                Y_lim_inf_FP_gauche, X_lim_inf_FP_gauche = 0,0
                Y_lim_inf_FP_droite, X_lim_inf_FP_droite = 0,0
            try:
                Y_fon, X_fon = float(self.data_equiv['Y'].loc[self.data_equiv['com'].str.lower()==self.fond_label.lower()]), float(self.data_equiv['X'].loc[self.data_equiv['com'].str.lower()==self.fond_label.lower()])
            except:
                # print('no bottom label')
                Y_fon, X_fon = 0,0
            return (X_lim_sup_FP_gauche, Y_lim_sup_FP_gauche,
                    X_lim_inf_FP_gauche, Y_lim_inf_FP_gauche,
                    X_lim_sup_FP_droite, Y_lim_sup_FP_droite,
                    X_lim_inf_FP_droite, Y_lim_inf_FP_droite,
                    X_fon, Y_fon)
        else:
            print('No data')
            return

    def interpolate_Z(self,cote):
        """
        compute intersection between cote and section outside MC
        :param cote: altitude
        :return: (right Y interpolated, left Y interpolated ) for cote
        """
        rd, rg,_ = self.check_rives()
        sec = self.data_equiv

        # RD Side
        y_rd = float(sec['Y'].loc[sec['com']== rd])
        z_rd = float(sec['Z'].loc[sec['com'] == rd])
        # data_rd_side = sec.loc[sec['Y'] <= y_rd]
        data_rd_side = sec.loc[:sec['com'].str.contains(rd).idxmax()]
        # intersection pts
        X_intersec_pts, Y_intersec_pts, Z_intersec_pts = [], [], []
        for i in range(len(data_rd_side) - 1):
            if (cote <= data_rd_side['Z'].iloc[i] and cote >= data_rd_side['Z'].iloc[i + 1]):
                X_intersec_pts.append(((data_rd_side['X'].iloc[i], data_rd_side['X'].iloc[i + 1])))
                Y_intersec_pts.append(((data_rd_side['Y'].iloc[i], data_rd_side['Y'].iloc[i + 1])))
                Z_intersec_pts.append(((data_rd_side['Z'].iloc[i], data_rd_side['Z'].iloc[i + 1])))

            # take the closest
        dist = [y_rd - elt[1] for elt in Y_intersec_pts]
        if len(dist) > 0:
            index_min = dist.index(min(dist))
            Xd_cl = X_intersec_pts[index_min]
            Yd_cl = Y_intersec_pts[index_min]
            Zd_cl = Z_intersec_pts[index_min]
            # interpolate
            fx = interp1d(Zd_cl, Xd_cl)
            fy = interp1d(Zd_cl, Yd_cl)
            Yd_interp = float(fy(cote))
            Xd_interp = float(fx(cote))
        else:
            if z_rd < cote:
                Yd_interp = self.data_equiv['Y'].iloc[0]
                Xd_interp = self.data_equiv['X'].iloc[0]
            else:
                Yd_interp = float(self.data_equiv['Y'].loc[self.data_equiv['com'] == rd])
                Xd_interp = float(self.data_equiv['X'].loc[self.data_equiv['com'] == rd])

        # rg side :
        y_rg = float(sec['Y'].loc[sec['com'] == rg])
        z_rg = float(sec['Z'].loc[sec['com'] == rg])
        # data_rg_side = sec.loc[sec['Y'] >= y_rg]
        data_rg_side = sec.loc[sec['com'].str.contains(rg).idxmax():]
            # intersection pts
        X_intersec_pts, Y_intersec_pts, Z_intersec_pts = [], [], []
        for i in range(len(data_rg_side) - 1):
            if (cote >= data_rg_side['Z'].iloc[i] and cote <= data_rg_side['Z'].iloc[i + 1]):
                X_intersec_pts.append(((data_rg_side['X'].iloc[i], data_rg_side['X'].iloc[i + 1])))
                Y_intersec_pts.append(((data_rg_side['Y'].iloc[i], data_rg_side['Y'].iloc[i + 1])))
                Z_intersec_pts.append(((data_rg_side['Z'].iloc[i], data_rg_side['Z'].iloc[i + 1])))

            # take the closest
        dist = [elt[0] - y_rg for elt in Y_intersec_pts]
        if len(dist)>0:
            index_min = dist.index(min(dist))
            Xg_cl = X_intersec_pts[index_min]
            Yg_cl = Y_intersec_pts[index_min]
            Zg_cl = Z_intersec_pts[index_min]
                # interpolate
            fx = interp1d(Zg_cl, Xg_cl)
            fy = interp1d(Zg_cl, Yg_cl)
            Yg_interp = float(fy(cote))
            Xg_interp = float(fx(cote))
        else:
            if z_rg < cote:
                Yg_interp = self.data_equiv['Y'].iloc[-1]
                Xg_interp = self.data_equiv['X'].iloc[-1]
            else:
                Yg_interp = float(self.data_equiv['Y'].loc[self.data_equiv['com']==rg])
                Xg_interp = float(self.data_equiv['X'].loc[self.data_equiv['com']==rg])


        # return data_rd_side, data_rg_side
        return ((Xd_interp,Yd_interp),(Xg_interp,Yg_interp))


    def get_intersections(self,cote):
        """
        find intersection nodes between cote and section
        :param cote:
        :return:
        """

        sect = self.data

        # Get new Z index
        data_rd_side = sect.loc[:sect['com'].str.contains(self.fond_label).idxmax()]
        new_Z_rd_index = data_rd_side.iloc[(data_rd_side['Z'] - cote).abs().argsort()[:1]].index[0]

        data_rg_side = sect.loc[sect['com'].str.contains(self.fond_label).idxmax():]
        new_Z_rg_index = data_rg_side.iloc[(data_rg_side['Z'] - cote).abs().argsort()[:1]].index[0]

        return (new_Z_rd_index,new_Z_rg_index)



    def move_Rives(self,Znew):

        sect = self.data
        # get RD&RG position
        old_rd_Ind = sect.loc[sect['com'] == self.RD_label].index[0]
        old_rg_Ind = sect.loc[sect['com'] == self.RG_label].index[0]

        new_Z_rd_index, new_Z_rg_index = self.get_intersections(Znew)

        # change values
        sect.loc[old_rd_Ind, ['com']] = ''
        sect.loc[new_Z_rd_index, ['com']] = self.RD_label

        sect.loc[old_rg_Ind, ['com']] = ''
        sect.loc[new_Z_rg_index, ['com']] = self.RG_label


    def add_RG(self,Znew):
        if self.fond_label in list(self.data['com']):
            if self.RG_label not in list(self.data['com']):
                sect = self.data
                data_rg_side = sect.loc[sect['com'].str.contains(self.fond_label).idxmax():]
                new_Z_rg_index = data_rg_side.iloc[(data_rg_side['Z'] - Znew).abs().argsort()[:1]].index[0]

                sect.loc[new_Z_rg_index, ['com']] = self.RG_label
        else:
            print('You need fond label. You can added using add_Fond()')

    def add_RD(self,Znew):
        if self.fond_label in list(self.data['com']):
            if self.RD_label not in list(self.data['com']):
                sect = self.data
                data_rd_side = sect.loc[:sect['com'].str.contains(self.fond_label).idxmax()]
                new_Z_rd_index = data_rd_side.iloc[(data_rd_side['Z'] - Znew).abs().argsort()[:1]].index[0]

                sect.loc[new_Z_rd_index, ['com']] = self.RD_label
        else:
            print('You need fond label. You can added using add_Fond()')



    def add_Fond(self):
        """
        label FOND in minimum Z
        :return:
        """
        if self.fond_label not in list(self.data['com']):
            sect = self.data
            min_Z = min(sect['Z'])
            index_min = sect.loc[sect['Z'] == min_Z].index[0]
            sect.iloc[index_min, 3] = self.fond_label
            # sect.loc[sect['Z'] == min_Z, 'com'].iloc[0] =