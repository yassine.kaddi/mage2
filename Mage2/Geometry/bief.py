from Mage2.Files.file_ST import file_ST
from Mage2.Files.file_NET import file_NET
from Mage2.Files.file_INI import file_INI
from Mage2.Geometry.section import sections
from Mage2.Files.file_M import file_M

import matplotlib.pyplot as plt
import numpy as np

import os
import subprocess

alpha=0.4
mksize=5
color1,color2,color3,color4,color5 = 'tomato','slategray', 'royalblue','lime','black'
fontsize=18

class biefs:
    """
    - total_nb : total number of biefs
    - elements : the biefs read

    """
    def __init__(self,name,path, args):
        self.mode_name = name
        self.path = path
        self.total_nb = 1
        self.elements = {}
        self.args = args

    def read_biefs(self):
        net_file = file_NET(self.mode_name,self.path)
        net_file.read_file()

        self.total_nb = net_file.total_nb

        for elt in range(self.total_nb):
            bief_el = bief(self.mode_name,self.path, self.args)
            bief_el.file_name = net_file.bief_data[elt][0]
            bief_el.node_upstream = net_file.bief_data[elt][1]
            bief_el.node_downstream = net_file.bief_data[elt][2]
            bief_el.id = elt+1
            bief_el.read_bief_sections()
            bief_el.read_bief_meshes()
            bief_el.get_pk_lim()
            self.elements[elt]= bief_el

    def export_ST(self,max_len=11):
        for el in self.elements:
            self.elements[el].export_ST(max_len)

class bief:
    """
    - sections : sections of ST file of the bief
    - mesh : sections of M file of the bief
    -file_name : name of ST file
    - node_upstream : upstream node name of the bief
    - node_downstream : downstream node name of the bief
    - rug : roughness of the bief
    - psi : PSI of the bief
    - pk_lim : XXX


    - read_bief_sections : read ST file for the "bief"
    - read_bief_meshes : read M file for the "bief"
    - export_ST : export ST file
    - export_INI : export INI file
    - run_mesh : run mesh command
    - top_view_section : plot top view of bief with initial cross sections (without meshing)
    - top_view_meshes : plot top view of bief with all cross sections (including meshing)
    - label_Fond : label Fond in all sections in their minimum Z
    """

    def __init__(self,name,path,args):
        self.model_name = name
        self.path = path
        self.file_name = ''
        self.node_upstream = ''
        self.node_downstream = ''
        self.rug = []  # [[(xmin,xmax),(Kmin,Kmoy)],]
        self.psi = []  # [[(xmin,xmax),psi],]
        self.id = 1
        self.pk_lim = ()
        self.hyd = 0
        self.hyd_time=None
        self.lim = 0
        self.args= args

    def read_bief_sections(self):
        self.sections = sections(self.model_name, self.path, self.args)
        self.sections.file_name = self.file_name+'.ST'
        self.sections.read_sections()

    def read_bief_meshes(self):
        self.mesh = file_M(self.model_name, self.path, self.args)
        self.mesh.file_name = self.file_name + '.M'
        self.mesh.read_meshes()

    def label_Fond(self):
        for pk in self.sections.elements:
            self.sections.elements[pk].add_Fond()

    def label_RG(self,Znew):
        for pk in self.sections.elements:
            self.sections.elements[pk].add_RG(Znew)

    def label_RD(self, Znew):
        for pk in self.sections.elements:
            self.sections.elements[pk].add_RD(Znew)

    def export_ST(self,max_len,verbose=True):
        if len(self.sections.elements) == 0:
            print('No cross-sections')
            return
        if len(self.path)==0:
            print('Pas de path!')
        else:
            ST_file = file_ST()
            ST_file.name = 'Bief_' + str(self.id) + '.ST'
            path_ST = self.path + '/MailleurPF/' + ST_file.name
            with open(path_ST, 'w') as file:
                [file.write(ST_file.write_file(self.sections.elements[sect], max_len)) for sect in self.sections.elements]

        if verbose:
            print('ST File exported')

    def get_pk_lim(self):
        if len(self.sections.elements) == 0:
            print('No cross-sections')
            return

        self.pk_lim = (min([sec for sec in self.sections.elements]), max([sec for sec in self.sections.elements]))

    def get_RD_RG_fon(self):
        self.read_bief_sections()
        key= list(self.sections.elements.keys())[0]
        first_section = self.sections.elements[key]
        args = first_section.RD_label, first_section.RG_label, first_section.fond_label
        ST_file = file_ST(args)
        RD,RG,fon = ST_file.get_data_meshing(self.path+'/MailleurPF/'+self.file_name+'.M')
        return RD,RG,fon

    def run_mesh(self,resolution):
        # print("Alert: mesh is run with command 'mesh3'")
        os.chdir(self.path+'/MailleurPF/')
        p = subprocess.Popen(['mesh3',self.file_name+'.ST'],stdin=subprocess.PIPE,stdout=subprocess.PIPE)
        p.communicate(input='{}'.format(resolution).encode())

    def export_INI(self,depth,fond_indicator,discharge,option='CC',option2='w'):
        '''
        export INI file for initialization
        :param depth: water depth
        :param fond_indicator: comment of the bottom
        :param discharge:
        :param option: 'CC' for Cote constante or 'HC' for Hauteur constante
        :return: exported INI file
        '''


        initialization = file_INI(self.model_name,self.path, self.args)
        initialization.depth = depth
        initialization.fond_indicator = fond_indicator
        initialization.discharge = discharge
        initialization.bief_nb = self.id
        initialization.export_INI(option,option2)

    def top_view_sections(self,path_save=''):
        """
        plot top view of bief
        :return:
        """

        Y_lim_sup_FP_gauche, X_lim_sup_FP_gauche = [], []
        Y_lim_inf_FP_gauche, X_lim_inf_FP_gauche = [], []
        Y_lim_sup_FP_droite, X_lim_sup_FP_droite = [], []
        Y_lim_inf_FP_droite, X_lim_inf_FP_droite = [], []
        Y_fon, X_fon = [], []

        all_sections = self.sections.elements
        for sec in all_sections:
            x1,y1,x2,y2,x3,y3,x4,y4,x5,y5 = all_sections[sec].get_limites()
            Y_lim_sup_FP_gauche.append(y1)
            X_lim_sup_FP_gauche.append(x1)

            Y_lim_inf_FP_gauche.append(y2)
            X_lim_inf_FP_gauche.append(x2)

            Y_lim_sup_FP_droite.append(y3)
            X_lim_sup_FP_droite.append(x3)

            Y_lim_inf_FP_droite.append(y4)
            X_lim_inf_FP_droite.append(x4)

            Y_fon.append(y5)
            X_fon.append(x5)

        plt.plot(X_lim_sup_FP_gauche, Y_lim_sup_FP_gauche, 'o', color=color1, alpha=alpha, markersize=mksize)
        plt.plot(X_lim_inf_FP_gauche, Y_lim_inf_FP_gauche, 'o', color=color1, alpha=alpha, markersize=mksize)
        plt.plot(X_fon, Y_fon, '--',color=color2)
        plt.plot(X_lim_sup_FP_droite, Y_lim_sup_FP_droite, 'o', color=color1, alpha=alpha, markersize=mksize)
        plt.plot(X_lim_inf_FP_droite, Y_lim_inf_FP_droite, 'o', color=color1, alpha=alpha, markersize=mksize)

        plt.fill(np.append(X_lim_sup_FP_gauche, X_lim_inf_FP_gauche[::-1]),
                 np.append(Y_lim_sup_FP_gauche, Y_lim_inf_FP_gauche[::-1]), alpha=alpha, color=color1, zorder=2)

        plt.fill(np.append(X_lim_sup_FP_droite, X_lim_inf_FP_droite[::-1]),
                 np.append(Y_lim_sup_FP_droite, Y_lim_inf_FP_droite[::-1]), alpha=alpha, color=color1, zorder=2)

        plt.fill(np.append(X_lim_sup_FP_droite, X_lim_inf_FP_gauche[::-1]),
                 np.append(Y_lim_sup_FP_droite, Y_lim_inf_FP_gauche[::-1]), alpha=alpha, color=color2, zorder=2)

        plt.xlabel('X (m)',fontsize=fontsize)
        plt.ylabel('Y (m)', fontsize=fontsize)
        if len(path_save)>0:
            plt.savefig(path_save)
        plt.show()



    def top_view_meshs(self,q_vals=[],fact=1,path_save=''):
        """
        plot top view of bief
        :param q_vals=[[ql],[qr]] list of q values in left interface ql and in right interface qr
               fact: factor multiplication to zoom q values
        :return:
        """

        Y_lim_sup_FP_gauche, X_lim_sup_FP_gauche = [], []
        Y_lim_inf_FP_gauche, X_lim_inf_FP_gauche = [], []
        Y_lim_sup_FP_droite, X_lim_sup_FP_droite = [], []
        Y_lim_inf_FP_droite, X_lim_inf_FP_droite = [], []
        Y_fon, X_fon = [], []

        all_sections = self.mesh.elements
        for sec in all_sections:
            x1,y1,x2,y2,x3,y3,x4,y4,x5,y5 = all_sections[sec].get_limites()
            Y_lim_sup_FP_gauche.append(y1)
            X_lim_sup_FP_gauche.append(x1)

            Y_lim_inf_FP_gauche.append(y2)
            X_lim_inf_FP_gauche.append(x2)

            Y_lim_sup_FP_droite.append(y3)
            X_lim_sup_FP_droite.append(x3)

            Y_lim_inf_FP_droite.append(y4)
            X_lim_inf_FP_droite.append(x4)

            Y_fon.append(y5)
            X_fon.append(x5)

        # plt.plot(X_lim_sup_FP_gauche, Y_lim_sup_FP_gauche, 'o', color=color1, alpha=alpha, markersize=mksize)
        # plt.plot(X_lim_inf_FP_gauche, Y_lim_inf_FP_gauche, 'o', color=color1, alpha=alpha, markersize=mksize)
        plt.plot(X_fon, Y_fon, '--',color=color2)
        # plt.plot(X_lim_sup_FP_droite, Y_lim_sup_FP_droite, 'o', color=color1, alpha=alpha, markersize=mksize)
        # plt.plot(X_lim_inf_FP_droite, Y_lim_inf_FP_droite, 'o', color=color1, alpha=alpha, markersize=mksize)

        plt.fill(np.append(X_lim_sup_FP_gauche, X_lim_inf_FP_gauche[::-1]),
                 np.append(Y_lim_sup_FP_gauche, Y_lim_inf_FP_gauche[::-1]), alpha=alpha, color=color1, zorder=2)

        plt.fill(np.append(X_lim_sup_FP_droite, X_lim_inf_FP_droite[::-1]),
                 np.append(Y_lim_sup_FP_droite, Y_lim_inf_FP_droite[::-1]), alpha=alpha, color=color1, zorder=2)

        plt.fill(np.append(X_lim_sup_FP_droite, X_lim_inf_FP_gauche[::-1]),
                 np.append(Y_lim_sup_FP_droite, Y_lim_inf_FP_gauche[::-1]), alpha=alpha, color=color2, zorder=2)

        # in case q values were given
        if len(q_vals)>0:
            Yg_arrow_sup, Xg_arrow_sup = [],[]
            Yd_arrow_sup, Xd_arrow_sup = [], []
            qg_vals,qd_vals = q_vals

            for x,y,qg in zip(X_lim_inf_FP_gauche,Y_lim_inf_FP_gauche,qg_vals):
                theta = np.arctan(y/x)
                dx,dy = -np.cos(theta)*qg*fact, np.sin(theta)*qg*fact
                # plt.arrow(x, y, dx, dy, head_width=0.3, head_length=0.3, color='black', length_includes_head=True)
                Yg_arrow_sup.append(y + dy)
                Xg_arrow_sup.append(x)
            plt.fill(np.append(X_lim_inf_FP_gauche, Xg_arrow_sup[::-1]), np.append(Y_lim_inf_FP_gauche, Yg_arrow_sup[::-1]), color=color5, alpha=.7)
            plt.plot(X_lim_inf_FP_gauche,Y_lim_inf_FP_gauche,color=color5)
            for x,y,qd in zip(X_lim_sup_FP_droite,Y_lim_sup_FP_droite,qd_vals):
                theta = np.arctan(y/x)
                dx,dy = -np.cos(theta)*qd*fact, np.sin(theta)*qd*fact
                # plt.arrow(x, y, dx, dy, head_width=0.3, head_length=0.3, color='black', length_includes_head=True)
                Yd_arrow_sup.append(y + dy)
                Xd_arrow_sup.append(x)
            plt.fill(np.append(X_lim_sup_FP_droite, Xd_arrow_sup[::-1]), np.append(Y_lim_sup_FP_droite, Yd_arrow_sup[::-1]), color=color5, alpha=.7)
            plt.plot(X_lim_sup_FP_droite,Y_lim_sup_FP_droite,color=color5)

        plt.xlabel('X (m)',fontsize=fontsize)
        plt.ylabel('Y (m)', fontsize=fontsize)
        if len(path_save)>0:
            plt.savefig(path_save)


        plt.show()

    def Z_view_sections(self,cote,path_save=''):
        """
        plot top view of bief at Z
        :param option: option=1 in case RG and RD were inversed in pamhyr. Otherwise: option=0
        :return:
        """
        print('option = 1 by default, otherwise choose 0')

        Y_lim_sup_FP_gauche, X_lim_sup_FP_gauche = [], []
        Y_lim_inf_FP_gauche, X_lim_inf_FP_gauche = [], []
        Y_lim_sup_FP_droite, X_lim_sup_FP_droite = [], []
        Y_lim_inf_FP_droite, X_lim_inf_FP_droite = [], []
        Y_fon, X_fon = [], []

        all_sections = self.sections.elements
        for sec in all_sections:
            x1,y1,x2,y2,x3,y3,x4,y4,x5,y5 = all_sections[sec].get_Z_limites(cote)
            Y_lim_sup_FP_gauche.append(y1)
            X_lim_sup_FP_gauche.append(x1)

            Y_lim_inf_FP_gauche.append(y2)
            X_lim_inf_FP_gauche.append(x2)

            Y_lim_sup_FP_droite.append(y3)
            X_lim_sup_FP_droite.append(x3)

            Y_lim_inf_FP_droite.append(y4)
            X_lim_inf_FP_droite.append(x4)

            Y_fon.append(y5)
            X_fon.append(x5)

        plt.plot(X_lim_sup_FP_gauche, Y_lim_sup_FP_gauche, 'o', color=color1, alpha=alpha, markersize=mksize)
        plt.plot(X_lim_inf_FP_gauche, Y_lim_inf_FP_gauche, 'o', color=color1, alpha=alpha, markersize=mksize)
        plt.plot(X_fon, Y_fon, '--',color=color2)
        plt.plot(X_lim_sup_FP_droite, Y_lim_sup_FP_droite, 'o', color=color1, alpha=alpha, markersize=mksize)
        plt.plot(X_lim_inf_FP_droite, Y_lim_inf_FP_droite, 'o', color=color1, alpha=alpha, markersize=mksize)

        plt.fill(np.append(X_lim_sup_FP_gauche, X_lim_inf_FP_gauche[::-1]),
                 np.append(Y_lim_sup_FP_gauche, Y_lim_inf_FP_gauche[::-1]), alpha=alpha, color=color1, zorder=2)

        plt.fill(np.append(X_lim_sup_FP_droite, X_lim_inf_FP_droite[::-1]),
                 np.append(Y_lim_sup_FP_droite, Y_lim_inf_FP_droite[::-1]), alpha=alpha, color=color1, zorder=2)

        plt.fill(np.append(X_lim_sup_FP_droite, X_lim_inf_FP_gauche[::-1]),
                 np.append(Y_lim_sup_FP_droite, Y_lim_inf_FP_gauche[::-1]), alpha=alpha, color=color2, zorder=2)

        plt.xlabel('X (m)',fontsize=fontsize)
        plt.ylabel('Y (m)', fontsize=fontsize)
        if len(path_save)>0:
            plt.savefig(path_save)
        plt.show()

    def Z_view_meshs(self,cote,path_save=''):
            """
            plot Z view of meshes bief
            :param option: option=1 in case RG and RD were inversed in pamhyr. Otherwise: option=0
            :return:
            """
            print('option = 1 by default, otherwise choose 0')

            Y_lim_sup_FP_gauche, X_lim_sup_FP_gauche = [], []
            Y_lim_inf_FP_gauche, X_lim_inf_FP_gauche = [], []
            Y_lim_sup_FP_droite, X_lim_sup_FP_droite = [], []
            Y_lim_inf_FP_droite, X_lim_inf_FP_droite = [], []
            Y_fon, X_fon = [], []

            all_sections = self.mesh.elements
            for sec in all_sections:
                x1, y1, x2, y2, x3, y3, x4, y4, x5, y5 = all_sections[sec].get_Z_limites(cote)
                Y_lim_sup_FP_gauche.append(y1)
                X_lim_sup_FP_gauche.append(x1)

                Y_lim_inf_FP_gauche.append(y2)
                X_lim_inf_FP_gauche.append(x2)

                Y_lim_sup_FP_droite.append(y3)
                X_lim_sup_FP_droite.append(x3)

                Y_lim_inf_FP_droite.append(y4)
                X_lim_inf_FP_droite.append(x4)

                Y_fon.append(y5)
                X_fon.append(x5)

            # plt.plot(X_lim_sup_FP_gauche, Y_lim_sup_FP_gauche, 'o', color=color1, alpha=alpha, markersize=mksize)
            # plt.plot(X_lim_inf_FP_gauche, Y_lim_inf_FP_gauche, 'o', color=color1, alpha=alpha, markersize=mksize)
            plt.plot(X_fon, Y_fon, '--', color=color2)
            # plt.plot(X_lim_sup_FP_droite, Y_lim_sup_FP_droite, 'o', color=color1, alpha=alpha, markersize=mksize)
            # plt.plot(X_lim_inf_FP_droite, Y_lim_inf_FP_droite, 'o', color=color1, alpha=alpha, markersize=mksize)

            plt.fill(np.append(X_lim_sup_FP_gauche, X_lim_inf_FP_gauche[::-1]),
                     np.append(Y_lim_sup_FP_gauche, Y_lim_inf_FP_gauche[::-1]), alpha=alpha, color=color1, zorder=2)

            plt.fill(np.append(X_lim_sup_FP_droite, X_lim_inf_FP_droite[::-1]),
                     np.append(Y_lim_sup_FP_droite, Y_lim_inf_FP_droite[::-1]), alpha=alpha, color=color1, zorder=2)

            plt.fill(np.append(X_lim_sup_FP_droite, X_lim_inf_FP_gauche[::-1]),
                     np.append(Y_lim_sup_FP_droite, Y_lim_inf_FP_gauche[::-1]), alpha=alpha, color=color2, zorder=2)

            plt.xlabel('X (m)', fontsize=fontsize)
            plt.ylabel('Y (m)', fontsize=fontsize)
            if len(path_save)>0:
                plt.savefig(path_save)
            plt.show()
