from Mage2.Files.file_RUG import file_RUG
from Mage2.Files.file_PSI import file_PSI
from Mage2.Files.file_LIM import file_LIM
from Mage2.Files.file_HYD import file_HYD
from Mage2.Files.file_NET import file_NET
from Mage2.Files.file_NUM import file_NUM
from Mage2.Files.file_TRA import file_TRA
from Mage2.Files.file_Output import file_Output
from Mage2.Files.file_AVA import file_AVA
import matplotlib.pyplot as plt

from Mage2.Results.Magefin import Magefin
from Mage2.Results.csv_files import csv_files
from Mage2.utils import round_time, convert_str_hours


from Mage2.Geometry.section import section
from Mage2.Geometry.bief import biefs
from Mage2.Files.file_INI import file_INI

import os
import numpy as np
import subprocess
from tqdm.notebook import tqdm
import time
import signal

alpha=0.4
mksize=5
color1,color2,color3,color4,color5 = 'tomato','slategray', 'royalblue','lime','black'
fontsize=18


# To add:
# > modifier les top_view pour lire à partir des abscisses
# > ERR & OUtput (pdtemps / Fr / Résidu / Cr)
# > export ST après hcangement n'a pas marché
# > CLM par bief
# > INI file par bief
# > RUG reading in biefs
# > INI files dans bief ou dans Etude ? (j'ai run_INI dans Etude et export_INI dans bief)
# > autpmatiser file indicator ds file_INI.py
# > modify TRANSFERT_CRUE10 to convert branches to biefs


# RQS:
#     dans compute_section: calcul fait en se basant sur X (plutôt que pk): X0+slope*Z
#  fix to have unique id for sections
# id in ST files are not ranked

class Etude:
    """
    Class for a Mage study. Need the study name and path of the study.
    """

    def __init__(self,name='',path='',option='',args=('rg','rd', 'fon')):
        self.name = name
        self.path = path
        self.option = option
        self.Magefin = Magefin(name,path,option)
        self.RUG = file_RUG(name,path)
        self.mage_command = 'mymage31'
        self.LIM = file_LIM(name,path)
        self.HYD = file_HYD(name,path)
        self.TRA = file_TRA(name,path)
        self.PSI = file_PSI(name,path)
        self.INI = file_INI(name,path, args)
        self.output = file_Output(name,path)
        self.NET = file_NET(name,path)
        self.NUM = file_NUM(name,path)
        self.CSV = csv_files(name,path,option)
        self.sections = []
        self.biefs = biefs(name,path, args)


    def read_all(self,verbose=True):

        if verbose:
            print(' ')
            print("Don't forget to define mage_command : Etude.mage_command = 'mymage31'")
            print("Don't forget to define args=('rg','rd','fon') by default")
            print(' ')
            print(' -- Reading files of model {} , option {} -- '.format(self.name,self.option))

        try:
            self.Magefin.read_file()
        except:
            print('Problem reading Magefin...')

        try:
            self.NUM.read_file()
        except:
            print('Problem reading NUM file...')

        try:
            self.HYD.read_file()
            print('Alert: HYD values only for steady case.')
        except:
            print('Problem reading HYD file...')

        try:
            self.LIM.read_file()
            print('Alert: LIM values only for steady case.')
        except:
            print('Problem reading LIM file...')

        try:
            self.CSV.read_z_files()
        except:
            print('Problem reading Z file exported in CSV')

        try:
            self.CSV.read_qi_files()
        except:
            print('Problem reading qi file exported in CSV')

        try:
            self.RUG.read_file() # In bief
        except:
            print('Problem reading RUG file')

        try:
            self.output.read_file() # In bief
        except:
            print('Problem reading output file')

        try:
            self.NET.read_file()
        except:
            print('Problem in reading NET file')

        try:
            self.biefs.read_biefs()
        except:
            print('Problem reading biefs')

        try:
            self.TRA.read_file()
        except:
            print('Problem in reading TRA file')


    # À MODIFIER PR PLS BIEFS
    def export_PSI(self):

        if len(self.biefs.elements) == 0:
            print('No bief')
            return

        path_PSI = self.path + '/' + self.PSI.file_name
        with open(path_PSI, 'w') as file:
            file.write(self.PSI.write_file(self.biefs))


    #À MODIFIER PR PLS BIEFS
    # def export_HYD(self):
    #
    #     if len(self.bief) == 0:
    #         print('No bief')
    #         return
    #
    #     CLM = [(bf.hyd,bf.node_upstream,bf.hyd_time) for bf in self.bief if bf.hyd !=0]
    #
    #     HYD_file = file_HYD()
    #     HYD_file.name = self.name + '.HYD'
    #
    #     path_HYD = self.path + '/' + HYD_file.name
    #
    #     with open(path_HYD, 'w') as file:
    #         [file.write(HYD_file.write_file(inlet[0],inlet[1],inlet[2])) for inlet in CLM]


    # À MODIFIER PR PLS BIEFS
    # def export_LIM(self):
    #
    #     if len(self.bief) == 0:
    #         print('No bief')
    #         return
    #
    #     LIM = [(bf.lim,bf.node_downstream) for bf in self.bief if bf.lim !=0]
    #
    #     LIM_file = file_LIM()
    #     LIM_file.name = self.name + '.LIM'
    #
    #     path_LIM = self.path + '/' + LIM_file.name
    #
    #     with open(path_LIM, 'w') as file:
    #         [file.write(LIM_file.write_file(inlet[0],inlet[1])) for inlet in LIM]

    def run_folders_with_u(self,u=.3,i=1):
        """
        run many Etude in many folders with -u option
        :param u:
        :param i:
        :return:
        """

        folders = os.listdir(self.path)
        t = time.time()
        results = []
        for fold in folders:
            results.append(self.run_Etude_with_u(self.path+'/'+fold,u,i))

        return results

    def run_folders_with_w(self,w=.3,i=1,verbose=False):
        """
        run many Etude in many folders with -w option
        :param u:
        :param i:
        :return:
        """

        folders = os.listdir(self.path)
        t = time.time()
        results = []
        for fold in folders:
            results.append(self.run_Etude_with_w(self.path+'/'+fold,w,i,verbose))

        return results




    def run_Etude_with_u(self,path,u=.3,i=1):
        """
        Run etude avec Mage
        :param w: poids
        :param i: =1 ou ISM ou 0 pour Debord
        :return:
        """
        os.chdir(path)
        p = subprocess.Popen(['{} -u={} -i={} {}'.format(self.mage_command, u, i, self.name)], stdin=subprocess.PIPE, stdout=subprocess.PIPE,shell=True, preexec_fn=os.setsid)
        OK_sim = ' >>>> Fin normale de MAGE <<<<'
        output, _ = p.communicate()
        output2 = output.decode('utf-8').split('\n')
        if OK_sim in output2:
            return True
        else:
            return False


    def run_Etude_with_w(self,path,w=.3,i=1,verbose=True):
        """
        Run etude avec Mage
        :param w: poids
        :param i: =1 ou ISM ou 0 pour Debord
        :return:
        """
        os.chdir(path)
        p = subprocess.Popen(['{} -w={} -i={} {}'.format(self.mage_command, w, i, self.name)], stdin=subprocess.PIPE, stdout=subprocess.PIPE,shell=True, preexec_fn=os.setsid)
        OK_sim = ' >>>> Fin normale de MAGE <<<<'
        output, _ = p.communicate()
        output2 = output.decode('utf-8').split('\n')

        if OK_sim in output2:
            if verbose:
                print("OK")
            return True
        else:
            if verbose:
                print("Didn't work")
            return False


    def run_INI(self, ini_val, w=.2, i=1):
        """
        to run mage =8 from script
        :param path: path of the files
        :param w: weight applied to MC velocity to compute interfaciel velocity
        :param i: ism (1) or debord (0)
        """
        os.chdir(self.path)
        p = subprocess.Popen(['{} -w={} -i={} {}'.format(self.mage_command, w, i, self.name)], stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
        OK_sim = ' >>>> Fin normale de MAGE <<<<'
        output, _ = p.communicate()
        output2 = output.decode('utf-8').split('\n')
        if OK_sim in output2:
            print('Maybe this INI {}'.format(ini_val))
        return self.get_time_simulation(output2)

    def find_INI(self,nb,option,stp,win,bief_id,discharge):
        _,_,Fond = self.biefs.elements[bief_id].get_RD_RG_fon()
        cases = np.arange(nb - win, nb + win, step=stp)
        times_sim = []
        self.INI.discharge = discharge
        for lim in tqdm(cases):
            if option == 'CC':
                self.INI.depth = lim
            else:
                self.INI.depth = lim - Fond['Z'].iloc[-1]
            self.INI.export_INI(option)
            time_sim=self.run_INI(round(self.INI.depth, 2))
            times_sim.append(time_sim)
        print('essayé pour: ', cases)
        return times_sim,cases

    def get_time_simulation(self,output):
        line = [elt for elt in output if 'Fin de la simulation' in elt][0]
        dt0 = [elt for elt in line.split(' ') if len(elt)>1][-1]
        dt = round_time(dt0)

        time_hour= convert_str_hours(dt)
        # if dt.split(':')[0] == '00':
        #     time_ = datetime.strptime(dt[3:], '%H:%M:%S')
        #     time_hour = round(time_.hour + time_.minute / 60 + time_.second / 3600, 4)
        # else:
        #     time_ = datetime.strptime(dt, '%d:%H:%M:%S')
        #     time_hour = round(time_.day * 24 + time_.hour + time_.minute / 60 + time_.second / 3600, 4)

        return time_hour


    def test_sections(self, sleep,w, mesh,max_len):
        '''
        run simulations by removing one section each run
        :return:  dictionary { section_removed : loss)
        '''

        result = {}
        best_sol = convert_str_hours(self.run_sim_loss(sleep, w, i=1))
        self.biefs.read_biefs()

        for BiefId in self.biefs.elements:
            bief_el = self.biefs.elements[BiefId]
            sections_copy = bief_el.sections.elements.copy()
            pk_list = list(sections_copy.keys())
            section_removed, losses = [], []
            for pk in tqdm(pk_list):
                bief_el.sections.elements.pop(pk)

                # export
                bief_el.export_ST(max_len,verbose=False)
                bief_el.run_mesh(mesh)

                # run sim & compute loss
                sim_time=self.run_sim_loss(sleep, w, i=1)
                sim_time_h = convert_str_hours(sim_time)
                losses.append(sim_time_h)
                section_removed.append(pk)

                if  sim_time_h > best_sol:
                    best_sol= sim_time_h
                    print('Best solution: Bief {}  //  section removed: {}  //  loss: {}  '.format(BiefId, pk,sim_time))
                    print(' ')

                # if sim_time_h == best_sol:
                    # print('same here')

                # initialize all_sects
                bief_el.sections.elements = sections_copy.copy()
                bief_el.export_ST(max_len, verbose=False)
                bief_el.run_mesh(mesh)


            result[BiefId]={'section_removed': section_removed, 'losses': losses}

        return result

    def test_sections2(self, sleep,w, mesh,max_len,start_bief=0):
        '''
        run simulations by removing 2 section each run
        :return:  dictionary { section_removed : loss)
        '''


        best_sol = convert_str_hours(self.run_sim_loss(sleep, w, i=1))
        self.biefs.read_biefs()

        biefs_el = np.arange(start_bief,len(self.biefs.elements.keys()))
        for BiefId in biefs_el:
            bief_el = self.biefs.elements[BiefId]
            sections_copy = bief_el.sections.elements.copy()
            pk_list = list(sections_copy.keys())
            pk_list3 = pk_list[:int(len(pk_list)/2)+1]
            section_removed, losses = [], []
            for pk in tqdm(pk_list3):
                bief_el.sections.elements.pop(pk)
                sections_copy2 = bief_el.sections.elements.copy()
                pk_list2 = pk_list.copy()
                pk_list2.remove(pk)
                for pk2 in pk_list2:
                    bief_el.sections.elements.pop(pk2)

                    # export
                    bief_el.export_ST(max_len,verbose=False)
                    bief_el.run_mesh(mesh)

                    # run sim & compute loss
                    sim_time=self.run_sim_loss(sleep, w, i=1)
                    sim_time_h = convert_str_hours(sim_time)
                    losses.append(sim_time_h)
                    # section_removed.append(pk2)

                    if  sim_time_h > best_sol:
                        best_sol = sim_time_h
                        print('Best solution: Bief {}  //  section1 removed: {}  //  section2 removed: {}  // loss: {}  \n'.format(
                            BiefId, pk, pk2, sim_time))
                        with open('test_sections_result.txt','a') as f:
                            f.write('Best solution: Bief {}  //  section1 removed: {}  //  section2 removed: {}  // loss: {}  \n'.format(
                            BiefId, pk, pk2, sim_time))
                            f.write(' ')

                    if sim_time_h == best_sol:
                        with open('test_sections_result.txt','a') as f:
                            f.write('same here  \n'.format(BiefId, pk, pk2, sim_time))
                            f.write(' ')

                    # initialize all_sects
                    bief_el.sections.elements = sections_copy2.copy()
                    bief_el.export_ST(max_len, verbose=False)
                    bief_el.run_mesh(mesh)

                # initialize all_sects
                bief_el.sections.elements = sections_copy.copy()
                bief_el.export_ST(max_len, verbose=False)
                bief_el.run_mesh(mesh)

            # result[BiefId]={'section_removed': section_removed, 'losses': losses}

        # return result

    def run_sim_loss(self, sleep, w, i=1):
        """
        run etude and gives loss compared with simulation time expected
        :param etude:
        :param w: weight in ISM simulation
        :param i:
        :return: loss
        """
        self.NUM.read_file()
        time_expected = self.NUM.params['final_time_hour']
        # Run
        os.chdir(self.path)

        p = subprocess.Popen(['{} -w={} -i={} {}'.format(self.mage_command, round(w, 2), i, self.name)],
                             stdin=subprocess.PIPE, stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
        time.sleep(sleep)  # sleep time is evaluated based on how much 1 simulation is supposed to last

        #  check if simulation started
        if self.output.simulation_stopped():
            os.killpg(p.pid, signal.SIGTERM)
            return 1

        # calculate obj function
        self.output.read_file()
        time_simulated = self.output.params['time_sim']

        os.killpg(p.pid, signal.SIGTERM)

        # return (time_expected - convert_str_hours(time_simulated)) / time_expected
        return time_simulated

    def top_view_biefs(self,list_biefs,path_save=''):
        """
        plot top view of bief
        :return:
        """

        for bif in list_biefs:

            Y_lim_sup_FP_gauche, X_lim_sup_FP_gauche = [], []
            Y_lim_inf_FP_gauche, X_lim_inf_FP_gauche = [], []
            Y_lim_sup_FP_droite, X_lim_sup_FP_droite = [], []
            Y_lim_inf_FP_droite, X_lim_inf_FP_droite = [], []
            Y_fon, X_fon = [], []

            all_sections = bif.mesh.elements
            for sec in all_sections:
                x1, y1, x2, y2, x3, y3, x4, y4, x5, y5 = all_sections[sec].get_limites()
                Y_lim_sup_FP_gauche.append(y1)
                X_lim_sup_FP_gauche.append(x1)

                Y_lim_inf_FP_gauche.append(y2)
                X_lim_inf_FP_gauche.append(x2)

                Y_lim_sup_FP_droite.append(y3)
                X_lim_sup_FP_droite.append(x3)

                Y_lim_inf_FP_droite.append(y4)
                X_lim_inf_FP_droite.append(x4)

                Y_fon.append(y5)
                X_fon.append(x5)

            plt.plot(X_fon, Y_fon, '--', color=color2)

            plt.fill(np.append(X_lim_sup_FP_gauche, X_lim_inf_FP_gauche[::-1]),
                     np.append(Y_lim_sup_FP_gauche, Y_lim_inf_FP_gauche[::-1]), alpha=alpha, color=color1, zorder=2)

            plt.fill(np.append(X_lim_sup_FP_droite, X_lim_inf_FP_droite[::-1]),
                     np.append(Y_lim_sup_FP_droite, Y_lim_inf_FP_droite[::-1]), alpha=alpha, color=color1, zorder=2)

            plt.fill(np.append(X_lim_sup_FP_droite, X_lim_inf_FP_gauche[::-1]),
                     np.append(Y_lim_sup_FP_droite, Y_lim_inf_FP_gauche[::-1]), alpha=alpha, color=color2, zorder=2)

        plt.xlabel('X (m)', fontsize=fontsize)
        plt.ylabel('Y (m)', fontsize=fontsize)
        if len(path_save)>0:
            plt.savefig(path_save)
        plt.show()