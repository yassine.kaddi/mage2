Le package Mage2 permet de manipuler les modèles 1D de Mage 8 (Debord & ISM). Les exemples d'usages du package sont détaillés dans 
le notebook Package_examples.ipynb. 
